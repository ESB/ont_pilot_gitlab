##############################
# Preprocessing

rule collect_nanostats:
    input:
        os.path.join(RESULTS_DIR, "qc/metag/lr/NanoStats.txt")
    output:
        os.path.join(RESULTS_DIR, "qc/metag/lr/NanoStats.tsv")
    run:
        import pandas
        from scripts.utils import parse_nanostats_report
        summary = parse_nanostats_report(input[0])
        summary.to_csv(output[0], sep="\t", header=True, index=False, index_label=False)

rule collect_fastp:
    input:
        os.path.join(RESULTS_DIR, "preproc/{mtype}/sr/fastp.json")
    output:
        os.path.join(RESULTS_DIR, "preproc/{mtype}/sr/fastp.tsv")
    wildcard_constraints:
        mtype="|".join(META_TYPES)
    run:
        import json
        import pandas
        with open(input[0], "r") as ifile:
            stats = json.load(ifile)
        stats = pandas.DataFrame.from_dict(stats["summary"], orient="index")
        stats.to_csv(output[0], sep="\t", header=True, index=True, index_label="label")

##############################
# Mapping

rule collect_mappability:
    input:
        idxstats=lambda wildcards: expand(
            os.path.join(RESULTS_DIR, "mapping/{{mtype}}/{rtype_tool}.idxstats.txt"),
            rtype_tool=["{r}/{t}/ASSEMBLY.POLISHED.{r}".format(r=r, t=t) for r, t in READ_ASSEMBLERS] if wildcards.mtype == "metag" else
            ["{r}/{t}/ASSEMBLY.POLISHED.sr".format(r=r, t=t) for r, t in READ_ASSEMBLERS]
        )
    output:
        os.path.join(RESULTS_DIR, "mapping/{mtype}/mappability.tsv")
    wildcard_constraints:
        mtype="|".join(META_TYPES)
    params:
        minlengths=[0, 1000, 2000, 5000]
    run:
        import pandas
        from scripts.utils import parse_samtools_idxstats_report
        summary = []
        for ifile in input.idxstats:
            # ifile_rtype = os.path.basename(os.path.dirname(os.path.dirname(ifile)))
            ifile_tool  = os.path.basename(os.path.dirname(ifile))
            ifile_df    = parse_samtools_idxstats_report(ifile, params.minlengths)
            # ifile_df    = ifile_df.assign(rtype=ifile_rtype)
            ifile_df    = ifile_df.assign(tool=ifile_tool)
            summary.append(ifile_df)
        summary = pandas.concat(objs=summary, axis="index")
        summary.to_csv(output[0], sep="\t", header=True, index=False, index_label=False, na_rep="NA")

##############################
# Annotation

rule collect_prodigal:
    input:
        expand(
            os.path.join(RESULTS_DIR, "annotation/prodigal/{rtype_tool}/proteins.faa"),
            rtype_tool=["%s/%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS]
        )
    output:
        gcounts=os.path.join(RESULTS_DIR, "annotation/prodigal/summary.gene.counts.tsv"),
        glength=os.path.join(RESULTS_DIR, "annotation/prodigal/summary.gene.length.tsv")
    run:
        import re
        import pandas
        gcounts = []
        with open(output.glength, "w") as ofile:
            ofile.write("tool\tgene_length\n")
            for ifile_path in input:
                # ifile_rtype     = os.path.basename(os.path.dirname(os.path.dirname(ifile_path)))
                ifile_tool      = os.path.basename(os.path.dirname(ifile_path))
                ifile_total     = 0
                ifile_partial   = 0
                with open(ifile_path, "r") as ifile:
                    for line in ifile:
                        if not re.match(">", line):
                            continue
                        sid, start, end, strand, attrs = line.strip().split(" # ")
                        # update counts
                        ifile_total += 1
                        if not re.search(";partial=00;", attrs):
                            ifile_partial += 1
                        # gene length
                        assert int(start) > 0 and int(end) > 0
                        # ofile.write("%s\t%s\t%d\n" % (ifile_rtype, ifile_tool, abs(int(end) - int(start)) + 1))
                        ofile.write("%s\t%d\n" % (ifile_tool, abs(int(end) - int(start)) + 1))
                # gcounts.append({"rtype": ifile_rtype, "tool": ifile_tool, "total": ifile_total, "partial": ifile_partial})
                gcounts.append({"tool": ifile_tool, "total": ifile_total, "partial": ifile_partial})
        gcounts = pandas.DataFrame(gcounts)
        gcounts.to_csv(output[0], sep="\t", header=True, index=False, index_label=False)
    # shell:
    #     """
    #     echo -e \"tool\\ttotal\\tpartial\" > {output.gcounts}
    #     for f in {input}
    #     do
    #         echo -e \"$(basename $(dirname ${{f}}))\\t$(grep '^>' ${{f}} | wc -l)\\t$(grep '^>' ${{f}} | grep 'partial=\(10\|01\|11\);' | wc -l)\"
    #     done >> {output.gcounts}
    #     echo -e \"tool\\tgene_length\" > {output.glength}
    #     for f in {input}
    #     do
    #         grep "^>" ${{f}} | sed 's/\s#\s/\\t/g' | awk -F'\\t' -v tool=\"$(basename $(dirname ${{f}}))\" '{{print tool\"\\t\"($3-$2+1) }}'
    #     done >> {output.glength}
    #     """

rule collect_plasflow:
    input:
        expand(
            os.path.join(RESULTS_DIR, "annotation/plasflow/{rtype_tool}/plasflow.tsv"),
            rtype_tool=["%s/%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS]
        )
    output:
        os.path.join(RESULTS_DIR, "annotation/plasflow/summary.tsv")
    run:
        import pandas
        from scripts.utils import parse_plasflow_report
        summary = []
        for ifile in input:
            idf_sum          = parse_plasflow_report(ifile)
            # idf_sum["rtype"] = os.path.basename(os.path.dirname(os.path.dirname(ifile)))
            idf_sum["tool"]  = os.path.basename(os.path.dirname(ifile))
            summary.append(idf_sum)
        summary = pandas.concat(objs=summary, axis="index")
        summary.to_csv(output[0], sep="\t", header=True, index=True, index_label="label")

rule collect_barrnap:
    input:
        expand(
            os.path.join(RESULTS_DIR, "annotation/barrnap/{rtype_tool}/{kingdom}.gff"),
            rtype_tool=["%s/%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS],
            kingdom=config["barrnap"]["kingdom"]
        )
    output:
        os.path.join(RESULTS_DIR, "annotation/barrnap/summary.tsv")
    run:
        import re
        import pandas
        # https://m.ensembl.org/info/website/upload/gff3.html
        gff_header = ["seqid", "source", "type", "start", "end", "score", "strand", "phase", "attrs"]
        summary = []
        for ifile in input:
            idf             = pandas.read_csv(ifile, sep="\t", header=None, names=gff_header, comment='#')
            idf["attrs"]    = idf["attrs"].apply(lambda x: re.sub("^product=", "", x.split(";")[1]))
            idf             = idf.groupby(["attrs"]).size()
            idf             = pandas.DataFrame({"gene": idf.index, "count": list(idf)})
            # idf["rtype"]    = os.path.basename(os.path.dirname(os.path.dirname(ifile)))
            idf["tool"]     = os.path.basename(os.path.dirname(ifile))
            idf["kingdom"]  = os.path.splitext(os.path.basename(ifile))[0]
            summary.append(idf)
        summary = pandas.concat(objs=summary, axis="index")
        summary.to_csv(output[0], sep="\t", header=True, index=False, index_label=False)

rule collect_rgi:
    input:
        expand(
            os.path.join(RESULTS_DIR, "annotation/rgi/{rtype_tool}/rgi.txt"),
            rtype_tool=["%s/%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS]
        )
    output:
        os.path.join(RESULTS_DIR, "annotation/rgi/summary.tsv")
    run:
        import pandas
        from scripts.utils import parse_rgi_report, merge_rgi_reports

        summary_types = ["all", "strict", "nudged"]
        summary_cols  = ["Best_Hit_ARO", "ARO", "Drug Class", "Resistance Mechanism", "AMR Gene Family"]
        
        # collect
        summaries = []
        for ifile in input:
            # ifile_rtype = os.path.basename(os.path.dirname(os.path.dirname(ifile)))
            ifile_tool  = os.path.basename(os.path.dirname(ifile))
            ifile_df    = parse_rgi_report(ifile, ifile_tool, summary_cols)
            # if there are hits
            if ifile_df is not None:
                summaries.append(ifile_df)
        if len(summaries) > 0:
            # merge
            merged_summaries = []
            for summary_col in summary_cols:
                for summary_type in summary_types:
                    merged_summaries.append(merge_rgi_reports(summaries, summary_col, summary_type))
            # concat
            summary = pandas.concat(objs=merged_summaries, axis="index")
            # reorder columns
            summary_cols_ordered = ["label", "col", "type"] + [c for c in summary.columns if c not in ["label", "col", "type"]]
            # save
            summary[summary_cols_ordered].to_csv(output[0], sep="\t", header=True, index=False, index_label=False, na_rep=0)
        else: # no hits -> empty file (onlt a header)
            with open(output[0], "w") as ofile:
                ofile.write("\t".join(["label", "col", "type"] + [os.path.basename(os.path.dirname(ifile)) for ifile in input]) + "\n")

rule collect_casc:
    input:
        expand(
            os.path.join(RESULTS_DIR, "annotation/casc/{rtype_tool}/casc.txt"),
            rtype_tool=["%s/%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS]
        )
    output:
        os.path.join(RESULTS_DIR, "annotation/casc/summary.tsv")
    run:
        import pandas
        from scripts.utils import parse_casc_report
        summary = []
        for ifile in input:
            # ifile_rtype = os.path.basename(os.path.dirname(os.path.dirname(ifile)))
            ifile_tool  = os.path.basename(os.path.dirname(ifile))
            ifile_df = parse_casc_report(ifile)
            # ifile_df = ifile_df.assign(crispr_tool="casc")
            ifile_df = ifile_df.assign(tool=ifile_tool)
            summary.append(ifile_df)
        summary = pandas.concat(objs=summary, axis="index")
        summary.to_csv(output[0], sep="\t", header=True, index=False, index_label=False)

rule collect_minced:
    input:
        expand(
            os.path.join(RESULTS_DIR, "annotation/minced/{rtype_tool}/minced.txt"),
            rtype_tool=["%s/%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS]
        )
    output:
        os.path.join(RESULTS_DIR, "annotation/minced/summary.tsv")
    run:
        import pandas
        from scripts.utils import parse_minced_report
        summary = []
        for ifile in input:
            # ifile_rtype = os.path.basename(os.path.dirname(os.path.dirname(ifile)))
            ifile_tool  = os.path.basename(os.path.dirname(ifile))
            ifile_df = parse_minced_report(ifile)
            # ifile_df = ifile_df.assign(crispr_tool="minced")
            ifile_df = ifile_df.assign(tool=ifile_tool)
            summary.append(ifile_df)
        summary = pandas.concat(objs=summary, axis="index")
        summary.to_csv(output[0], sep="\t", header=True, index=False, index_label=False)

##############################
# Analysis

rule collect_quast:
    input:
        expand(
            os.path.join(RESULTS_DIR, "analysis/quast/{rtype_tool}/report.tsv"),
            rtype_tool=["%s/%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS]
        )
    output:
        os.path.join(RESULTS_DIR, "analysis/quast/summary_report.tsv")
    run:
        import pandas
        summary = None
        for ifile in input:
            # ifile_rtype = os.path.basename(os.path.dirname(os.path.dirname(ifile)))
            ifile_tool  = os.path.basename(os.path.dirname(ifile))
            ifile_df    = pandas.read_csv(ifile, sep="\t", header=0, index_col=0)
            ifile_df.rename(columns={"ASSEMBLY.POLISHED": ifile_tool}, inplace=True)
            if summary is None:
                summary = ifile_df.copy()
            else:
                summary = summary.merge(
                    right=ifile_df,
                    how="outer",
                    left_index=True,
                    right_index=True,
                    sort=False
                )
        summary.to_csv(output[0], sep="\t", header=True, index=True, index_label=False)

# rule collect_mashmap: TODO

rule collect_mummer:
    input:
        expand(
            os.path.join(RESULTS_DIR, "analysis/mummer/{combi}.dnadiff.report"),
            combi=["%s_%s__%s_%s" % (p[0][0], p[0][1], p[1][0], p[1][1]) for p in READ_ASSEMBLER_PAIRS] +
            ["%s_%s__%s_%s" % (p[1][0], p[1][1], p[0][0], p[0][1]) for p in READ_ASSEMBLER_PAIRS]
        )
    output:
        os.path.join(RESULTS_DIR, "analysis/mummer/summary.dnadiff.tsv")
    shell:
        # NOTE: collecting stat.s for REF only, NOT for QRY
        #       since all combinations are processed anyway and
        #       the numbers are almost the same for both combinations (tool1/tool2 and tool2/tool1)
        """
        echo -e 'tool1\\ttool2\\tseqs_total\\tseqs_aligned\\tbases_total\\tbases_aligned' > {output}
        for f in {input}; do
            tool1=$(basename -s '.dnadiff.report' ${{f}} | sed 's/__/,/' | cut -d',' -f1 | cut -d '_' -f2)
            tool2=$(basename -s '.dnadiff.report' ${{f}} | sed 's/__/,/' | cut -d',' -f2 | cut -d '_' -f2)
            seqs=$(grep -A 2 '^\[Sequences\]' ${{f}} | tail -n +2 | sed 's/\s\+/\\t/g' | cut -f2 | sed 's/([0-9]\+\.[0-9]\+%)//g' | tr '\\n' '\\t' | sed 's/\s\+$//')
            bases=$(grep -A 2 '^\[Bases\]' ${{f}} | tail -n +2 | sed 's/\s\+/\\t/g' | cut -f2 | sed 's/([0-9]\+\.[0-9]\+%)//g' | tr '\\n' '\\t' | sed 's/\s\+$//')
            echo -e \"${{tool1}}\\t${{tool2}}\\t${{seqs}}\\t${{bases}}\"
        done >> {output}
        """

rule collect_diamondDB:
    input:
        expand(
            os.path.join(RESULTS_DIR, "analysis/diamond/{rtype_tool}.tsv"),
            rtype_tool=["%s_%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS]
        )
    output:
        os.path.join(RESULTS_DIR, "analysis/diamond/summary.db.tsv")
    run:
        import pandas
        from scripts.utils import parse_diamond_tsv
        summary = []
        for ifile in input:
            # ifile_rtype = os.path.splitext(os.path.basename(ifile))[0].split("_")[0]
            ifile_tool  = os.path.splitext(os.path.basename(ifile))[0].split("_")[1]
            ifile_df    = parse_diamond_tsv(ifile)
            ifile_df    = ifile_df.assign(tool=ifile_tool)
            summary.append(ifile_df[["qs_len_ratio", "qcov", "scov", "tool"]].copy())
        summary = pandas.concat(objs=summary, axis="index")
        summary.to_csv(output[0], sep="\t", header=True, index=False, index_label=False)

rule collect_diamond_stats:
    input:
        tsv=os.path.join(RESULTS_DIR, "analysis/diamond/{rtype1}_{tool1}__{rtype2}_{tool2}.tsv"),
        gcov=os.path.join(RESULTS_DIR, "mapping/{mtype}/{rtype2}/{tool2}/ASSEMBLY.POLISHED.sr.cov.pergene")
    output:
        temp(os.path.join(RESULTS_DIR, "analysis/diamond/{rtype1}_{tool1}__{rtype2}_{tool2}.{mtype}.stats"))
    wildcard_constraints:
        mtype="|".join(META_TYPES),
        rtype1="|".join(READ_TYPES),
        rtype2="|".join(READ_TYPES),
        tool1="|".join(ASSEMBLERS),
        tool2="|".join(ASSEMBLERS),
    params:
        mincov=10
    run:
        from scripts.utils import parse_diamond_tsv
        gcov = pandas.read_csv(input.gcov, sep="\t", header=0)
        # high-cov. genes (proteins)
        hgenes = list(gcov.loc[gcov["ave_cov"] >= params.mincov,"prot_id"])
        # unique genes (proteins) = all w/o those with hits
        hits = parse_diamond_tsv(input.tsv)
        ugenes = set(gcov["prot_id"]).difference(list(hits["qseqid"]))
        # summary
        df = pandas.DataFrame.from_dict({
            "tool1": [os.path.splitext(os.path.basename(input.tsv))[0].split("__")[0].split("_")[1]],
            "tool2": [os.path.splitext(os.path.basename(input.tsv))[0].split("__")[1].split("_")[1]],
            "total": [gcov.shape[0]],
            "uniq": [len(ugenes)],
            "highcov": [len(hgenes)],
            "highcovuniq": [len(set(ugenes).intersection(hgenes))]
        }, orient="columns")
        df.to_csv(output[0], sep="\t", header=True, index=False, index_label=False)

rule collect_diamond:
    input:
        expand(
            os.path.join(RESULTS_DIR, "analysis/diamond/{combi}.{{mtype}}.stats"),
            combi=["%s_%s__%s_%s" % (p[0][0], p[0][1], p[1][0], p[1][1]) for p in READ_ASSEMBLER_PAIRS] +
            ["%s_%s__%s_%s" % (p[1][0], p[1][1], p[0][0], p[0][1]) for p in READ_ASSEMBLER_PAIRS]
        ),
    output:
        os.path.join(RESULTS_DIR, "analysis/diamond/summary.{mtype}.tsv")
    wildcard_constraints:
        mtype="|".join(META_TYPES)
    shell:
        """
        head -n 1 {input[0]} > {output}
        for f in {input}; do tail -n +2 ${{f}}; done >> {output}
        """

rule collect_cdhit_stats:
    input:
        faa=os.path.join(RESULTS_DIR, "analysis/cdhit/{rtype1}_{tool1}__{rtype2}_{tool2}.faa"),
        gcov=os.path.join(RESULTS_DIR, "mapping/{mtype}/{rtype2}/{tool2}/ASSEMBLY.POLISHED.sr.cov.pergene")
    output:
        temp(os.path.join(RESULTS_DIR, "analysis/cdhit/{rtype1}_{tool1}__{rtype2}_{tool2}.{mtype}.stats"))
    wildcard_constraints:
        mtype="|".join(META_TYPES),
        rtype1="|".join(READ_TYPES),
        rtype2="|".join(READ_TYPES),
        tool1="|".join(ASSEMBLERS),
        tool2="|".join(ASSEMBLERS),
    params:
        mincov=10
    run:
        import os
        import re
        import pandas
        gcov = pandas.read_csv(input.gcov, sep="\t", header=0)
        # high-cov. genes (proteins)
        hgenes = list(gcov.loc[gcov["ave_cov"] >= params.mincov,"prot_id"])
        # unique genes (proteins)
        ugenes = []
        with open(input.faa, "r") as ifile:
            for line in ifile:
                if not re.match(">", line):
                    continue
                pid = "__".join(line.strip().split("__")[2:]).split(" ")[0]
                # assert pid in gcov["prot_id"].values, "could not find {} in {}".format(pid, input.gcov)
                ugenes.append(pid)
        # summary
        df = pandas.DataFrame.from_dict({
            "tool1": [os.path.splitext(os.path.basename(input.faa))[0].split("__")[0].split("_")[1]],
            "tool2": [os.path.splitext(os.path.basename(input.faa))[0].split("__")[1].split("_")[1]],
            "total": [gcov.shape[0]],
            "uniq": [len(ugenes)],
            "highcov": [len(hgenes)],
            "highcovuniq": [len(set(ugenes).intersection(hgenes))]
        }, orient="columns")
        df.to_csv(output[0], sep="\t", header=True, index=False, index_label=False)

rule collect_cdhit:
    input:
        expand(
            os.path.join(RESULTS_DIR, "analysis/cdhit/{combi}.{{mtype}}.stats"),
            combi=["%s_%s__%s_%s" % (p[0][0], p[0][1], p[1][0], p[1][1]) for p in READ_ASSEMBLER_PAIRS] +
            ["%s_%s__%s_%s" % (p[1][0], p[1][1], p[0][0], p[0][1]) for p in READ_ASSEMBLER_PAIRS]
        ),
    output:
        os.path.join(RESULTS_DIR, "analysis/cdhit/summary.{mtype}.tsv")
    wildcard_constraints:
        mtype="|".join(META_TYPES)
    shell:
        """
        head -n 1 {input[0]} > {output}
        for f in {input}; do tail -n +2 ${{f}}; done >> {output}
        """

rule collect_mmseqs2:
    input:
        os.path.join(RESULTS_DIR, "analysis/mmseqs2/clusters.tsv")
    output:
        os.path.join(RESULTS_DIR, "analysis/mmseqs2/summary.tsv")
    run:
        from scripts.utils import mmseqs2_tsv, mmseqs2_summary
        counts  = mmseqs2_tsv(input[0])
        summary = mmseqs2_summary(counts)
        summary.to_csv(output[0], sep="\t", header=True, index=True, index_label="combination")

rule collect_segsum:
    input:
        expand(
            os.path.join(RESULTS_DIR, "mapping/metag/{rtype_tool}/ASSEMBLY.POLISHED.sr.cov.segsum.tsv"),
            rtype_tool=["%s/%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS]
        )
    output:
        os.path.join(RESULTS_DIR, "mapping/summary.segsum.metag.tsv")
    run:
        import pandas
        summary = []
        for ifile in input:
            # ifile_rtype = os.path.basename(os.path.dirname(os.path.dirname(ifile)))
            ifile_tool  = os.path.basename(os.path.dirname(ifile))
            ifile_df    = pandas.read_csv(ifile, sep="\t", header=0)
            ifile_df    = ifile_df.assign(tool=ifile_tool)
            summary.append(ifile_df.copy())
        summary = pandas.concat(objs=summary, axis="index")
        summary.to_csv(output[0], sep="\t", header=True, index=False, index_label=False)