#!/usr/bin/env python

# NOTE:
#   mash screen tutorial: https://mash.readthedocs.io/en/latest/tutorials.html#screening-a-read-set-for-containment-of-refseq-genomes
#   columns: identity, shared-hashes, median-multiplicity, p-value, query-ID, query-comment

import subprocess

for i in range(0, len(snakemake.input.fasta)):
    fa  = snakemake.input.fasta[:i] + snakemake.input.fasta[i+1:]
    print("mash screen: {} in {}".format(fa, snakemake.input.msh[i]))
    subprocess.check_call(
        "mash screen -p {p} -i -1 {m} {f} > {o}".format(
            p=snakemake.threads,
            m=snakemake.input.msh[i],
            f=" ".join(fa),
            o=snakemake.output[i]
        ),
        shell=True
    )