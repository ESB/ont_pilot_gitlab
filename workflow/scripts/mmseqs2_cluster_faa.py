#!/usr/bin/env python

# FAA file containing only cluster representatives

import pandas
from Bio import SeqIO

# protein IDs = cluster representatives (1st column)
prot_ids = set(pandas.read_csv(snakemake.input.tsv, sep="\t", header=None).iloc[:,0])
# filter input FAA by IDs
with open(snakemake.input.faa, "r") as ifile, open(snakemake.output[0], "w") as ofile:
    for record in SeqIO.parse(ifile, "fasta"):
        if record.id in prot_ids:
            SeqIO.write(record, ofile, "fasta")
