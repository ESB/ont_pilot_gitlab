## This method of depth calculation was adapted and modified from the CONCOCT code
BEGIN {{pc=""}}
{{
    c=$1;
    if (c == pc) {{
    cov=cov+$2*$5;
}} else {{
    print pc,cov;
    cov=$2*$5;
    pc=c}}
}} END {{print pc,cov}}
