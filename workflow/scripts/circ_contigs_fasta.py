#!/usr/bin/env python

from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

with open(snakemake.input[0], "r") as ifile, open(snakemake.output.split1, "w") as ofile1, open(snakemake.output.split2, "w") as ofile2:
    for record in SeqIO.parse(ifile, "fasta"):
        n = round(len(record.seq)/2)
        record_1 = SeqRecord(record.seq[0:n], id=record.id + "_1", name="", description="")
        record_2 = SeqRecord(record.seq[n:],  id=record.id + "_2", name="", description="")
        SeqIO.write(record_1, ofile1, "fasta")
        SeqIO.write(record_2, ofile2, "fasta")
