#!/usr/bin/env python

def find_fast5(file_list=[], dir_list=[]):
    """
    Create a list of FAST5 files from given list of files and folders
    FAST5 files are searched recursively in the given folders
    """
    import os
    from pathlib import Path
    # list of files
    fast5_files = []
    # files from list of files
    fast5_files += [os.path.abspath(f) for f in file_list]
    # files from list of directories
    for ont_d in dir_list:
        fast5_files += [os.path.abspath(f) for f in Path(ont_d).rglob('*.fast5')]
    # basenames should be unique
    assert len(fast5_files) == len(set([os.path.basename(f) for f in fast5_files])), "Basenames of FAST5 files are NOT unique: {}".format(fast5_files)
    return fast5_files

def assembler_pairs(assemblers):
    """
    Create all possible combinations of two assemblers
    """
    from itertools import combinations
    return list(combinations(assemblers, 2))

def parse_casc_report(ifile_path):
    """
    TODO
    """
    import pandas
    # read in
    summary = pandas.read_csv(ifile_path, sep="\t", header=0, usecols=["#seq_id", "spacers", "array_start", "array_stop", "bonafide"], comment=None)
    # rename columns
    summary.rename(columns={"#seq_id": "seq_id"}, inplace=True)
    # filter
    summary = summary[summary["bonafide"]]
    assert all(summary["bonafide"]) # just a check
    # drop columns
    summary.drop(columns=["bonafide"], inplace=True)
    return summary

def parse_minced_report(ifile_path):
    """
    Parse the MINCED report: for each sequence and found CRISPR array, extract
    - array start and end
    - number of spacers = number of unique spacer sequences

    Entry (sequence/CRISPR array) format:
        Sequence '<string>' (<int> bp)

        CRISPR <int>   Range: <int> - <int>
        POSITION        REPEAT                                  SPACER
        --------        ------------------------------------    ------------------------------
        <int>           <string: ACTG>                          <string: ACTG>                 [ <int>, <int> ]
        <...>
        <int>           <string: ACTG>    
        --------        ------------------------------------    ------------------------------
        Repeats: <int>      Average Length: <int>              Average Length: <int>

        CRISPR <int>   Range: <int> - <int>
        <...>

        Time to find repeats: <int> ms
    """
    import re
    import pandas
    summary = []
    with open(ifile_path, "r") as ifile:
        new_seq = 0
        seq_id = None
        for line in ifile:
            if re.match("Sequence ", line):
                assert new_seq == 0, "Unexpected parsing error in {} in {}".format(line, ifile_path)
                line_re = re.search(re.compile("^Sequence '(?P<seq_id>[\w\.:-]+)' \((?P<seq_len>\d+) bp\)$"), line)
                assert line_re, "Could not extract info from \"{}\" in {}".format(line, ifile_path)
                seq_id = line_re.group("seq_id")
                new_seq = 1
            elif re.match("CRISPR ", line):
                assert new_seq == 1, "No seq before {} in {}".format(line, ifile_path)
                line_re = re.search(re.compile("^CRISPR (?P<crispr_id>\d+)\s+Range: (?P<array_start>\d+) - (?P<array_stop>\d+)$"), line)
                assert line_re, "Could not extract info from \"{}\" in {}".format(line, ifile_path)
                summary.append({
                    "seq_id": seq_id,
                    "array_start": int(line_re.group("array_start")),
                    "array_stop": int(line_re.group("array_stop")),
                    "spacers": set()
                })
                new_seq += 1
            elif re.match("POSITION", line):
                assert new_seq == 2, "No new seq/CRISPR before {} in {}".format(line, ifile_path)
                new_seq += 1
            elif re.match("[0-9]+\s+", line):
                assert re.match("[0-9]+\s+[ACTG]+", line), "Unexpected repeat/spacer format in {} in {}".format(line, ifile_path)
                assert new_seq == 3, "No new seq./CRISPR before {} in {}".format(line, ifile_path)
                if re.match("[0-9]+\s+[ACTG]+\s+[ACTG]+\s+", line):
                    line_re = re.search(re.compile("^[0-9]+\s+(?P<repeat>[ACTG]+)\s+(?P<spacer>[ACTG]+)\s+.*$"), line)
                    assert line_re, "Could not extract repeat/spacer from \"{}\" in {}".format(line, ifile_path)
                    summary[len(summary)-1]["spacers"].add(line_re.group("spacer"))
            elif re.match("Repeats:", line):
                new_seq = 1
                summary[len(summary)-1]["spacers"] = len(summary[len(summary)-1]["spacers"])
            elif re.match("Time to find repeats", line):
                assert new_seq == 1 and summary[len(summary)-1]["spacers"] > 0, "No new seq./CRISPR/spacers before {} in {}".format(line, ifile_path)
                new_seq = 0
                seq_id = None
    summary = pandas.DataFrame(summary)
    return summary

def parse_plasflow_report(ifile_path):
    import pandas

    def sum_up(df, colname, label):
        df_sum = df.groupby(colname)['contig_length'].agg(['sum','count'])
        df_sum["count_pct"] = 100 * df_sum["count"] / sum(df_sum["count"])
        df_sum["sum_pct"]   = 100 * df_sum["sum"]   / sum(df_sum["sum"])
        df_sum["type"]      = label
        return df_sum

    idf = pandas.read_csv(ifile_path, sep="\t", header=0, usecols=["contig_length", "label"])
    # extract class label, i.e. w/o taxon
    idf = idf.assign(classlabel=idf.label.apply(lambda x: x.split(".")[0]))
    # sum up
    idf_labelsum = sum_up(idf, "label", "label")
    idf_classsum = sum_up(idf, "classlabel", "class")
    idf_sum = pandas.concat(objs=[idf_labelsum, idf_classsum], axis="index")
    return idf_sum

def parse_nanostats_report(ifile_path):
    """
    Parse the report from NanoStats
    """
    import re
    import pandas

    summary = dict()
    # file content
    flag = None
    with open(ifile_path, "r") as ifile:
        for line in ifile:
            if re.match("General summary", line):
                flag = "general"
            elif re.match("Number, percentage and megabases of reads above quality cutoffs", line):
                flag = "number"
            elif re.match("Top 5 highest mean basecall quality scores and their read lengths", line):
                flag = "top5QS"
            elif re.match("Top 5 longest reads and their mean basecall quality score", line):
                flag = "top5LR"
            else:
                # general summary
                if flag == "general":
                    line = re.sub(":\s+", "\t", line)
                    k, v = line.split("\t")
                    summary[k] = float(re.sub(",", "", v))
                # rest: skip
                else:
                    continue
    # dict to DataFrame
    summary = pandas.DataFrame(summary.items(), columns=['stat', 'value'])
    return summary

def parse_rgi_report(ifile_path, ifile_tool, summary_cols):
    import pandas
    import numpy

    def count_hits(df, colname, tool_label=None):
        """
        Sum up RGI results: count hits by grouping them
        """
        df_sum = df.groupby(colname)[colname].agg(["count"])
        df_sum["label"] = df_sum.index
        if tool_label is not None:
            df_sum.rename(columns={"count": tool_label}, inplace=True)
        return df_sum
    
    def sumup_hits(df, summary_cols, tool_label):
        """
        Count hits grouping them by each given column: all/strict/nudged
        """
        assert set(df.Nudged) == {numpy.nan, True}, "Unexpected \"Nudged\" value in RGI report: {}".format(set(df.Nudged))
        summary = dict.fromkeys(summary_cols)
        for summary_col in summary_cols:
            summary[summary_col] = {
                "all": count_hits(df, summary_col, tool_label),
                "strict": count_hits(df[pandas.isnull(df.Nudged)], summary_col, tool_label),
                "nudged": count_hits(df[pandas.notnull(df.Nudged)], summary_col, tool_label)
            }
        return summary
    
    idf = pandas.read_csv(ifile_path, sep="\t", header=0, usecols=["ORF_ID"] +  summary_cols + ["Nudged"])
    idf_sum = None
    if idf.shape[0] > 0:
        idf_sum = sumup_hits(idf, summary_cols, ifile_tool)
    return idf_sum

def merge_rgi_reports(dfs, summary_col, summary_type):
    """
    Merge summaries (list of DataFrame obj.s) for given column and type
    """
    summary = None
    for df in dfs:
        if summary is None:
                summary =  df[summary_col][summary_type].copy()
        else:
            summary = summary.merge(
                right=df[summary_col][summary_type],
                on="label",
                how="outer",
                sort=False
            )
    summary["col"]  = summary_col
    summary["type"] = summary_type
    return summary

# NOTE: currently not used; data from idxstats provides the same information
# def parse_samtools_flagstat_report(ifile_path):
#     import re
#     summary = {}
#     with open(ifile_path, "r") as ifile:
#         for line in ifile:
#             if re.search("^\d+ \+ \d+ in total \(", line):
#                 line_re = re.search(re.compile("^(?P<passed>\d+) \+ (?P<failed>\d+) .*$"), line)
#                 assert line_re, "Could not extract info from \"{}\" in {}".format(line, ifile_path)
#                 summary["total"] = int(line_re.group("passed"))
#             elif re.search("^\d+ \+ \d+ mapped \(", line):
#                 line_re = re.search(re.compile("^(?P<passed>\d+) \+ (?P<failed>\d+) .*$"), line)
#                 assert line_re, "Could not extract info from \"{}\" in {}".format(line, ifile_path)
#                 summary["mapped"] = int(line_re.group("passed"))
#         summary["mapped_pct"] = 100 * summary["mapped"] / summary["total"]
#     return summary

def parse_samtools_idxstats_report(ifile_path, minlengths):
    import pandas
    summary = pandas.DataFrame(columns=["variable", "value", "minlength"])
    df = pandas.read_csv(ifile_path, sep="\t", header=None, names=["seq_id", "seq_length", "mapped", "unmapped"])
    total = sum(df.mapped) + sum(df.unmapped)

    summary = summary.append({"variable": "total", "value": sum(df.mapped) + sum(df.unmapped), "minlength": None}, ignore_index=True)
    for minlength in minlengths:
        mapped     = sum(df[df.seq_length >= minlength].mapped)
        mapped_pct = round(100 * mapped / total, 2)
        summary = summary.append({"variable": "mapped",     "value": mapped,     "minlength": minlength}, ignore_index=True)
        summary = summary.append({"variable": "mapped_pct", "value": mapped_pct, "minlength": minlength}, ignore_index=True)
    summary = pandas.DataFrame(summary)
    return summary

def parse_diamond_tsv(ifile_path, columns=["qseqid", "sseqid", "pident", "length", "mismatch", "gapopen", "qstart", "qend", "sstart", "send", "evalue", "bitscore", "qlen", "slen"]):
    import pandas
    df = pandas.read_csv(ifile_path, sep="\t", header=None, names=columns)
    # best hit per query
    df_hits = df.sort_values(by=['qseqid', 'evalue'], axis=0, ascending=[True, True]).groupby('qseqid').head(1).reset_index(drop=True)
    # query/subject length ratio
    df_hits = df_hits.assign(qs_len_ratio=df_hits.qlen / df_hits.slen)
    # query/subject coverage
    df_hits = df_hits.assign(qcov=(abs(df_hits.qend - df_hits.qstart) + 1) / df_hits.qlen)
    df_hits = df_hits.assign(scov=(abs(df_hits.send - df_hits.sstart) + 1) / df_hits.slen)
    # df_hits = df_hits.assign(qs_cov_ratio=df_hits.qcov / df_hits.scov)
    return df_hits

def parse_prodigal_faa_headers(ifile_path):
    import re
    import pandas
    genes = []
    with open(ifile_path) as ifile:
        for line in ifile:
            if not re.match(">", line):
                continue
            line = re.sub("^>", "", line.strip())
            pid, pstart, pend, pstrand, pinfo = line.split(" # ")
            genes.append({
                "contig_id": "_".join(pid.split("_")[:-1]),
                "prot_num": pid.split("_")[-1],
                "prot_id": pid,
                "start": int(pstart),
                "end": int(pend),
                "strand": pstrand,
                "info": pinfo
            })
    genes = pandas.DataFrame(genes)
    assert (genes.start < genes.end).all() # TODO: TEST
    return genes

def ave_gene_cov(cov_file, genes):
    """
    Compute average coverage from given per-base coverage file and a list of genes
    Genes must be in a table as created by parse_prodigal_faa_headers()
    """
    genes = genes.sort_values(by=["contig_id", "prot_num"])
    genes = genes.assign(ave_cov=None)
    with open(cov_file, "r") as ifile:
        last_contig_id  = ""
        last_contig_cov = [] # array of cov. values for bases 1..N (idx 0..N-1)
        for line in ifile:
            contig_id, base, coverage = line.strip().split("\t")
            assert contig_id != ""
            if contig_id != last_contig_id:
                if last_contig_id != "": # "" at the beginning before having parsed the 1st contig
                    contig_genes = genes.contig_id == last_contig_id # genes/proteins of last contig
                    # ave. cov. (note: start/end w.r.t. Python indexing)
                    genes.loc[contig_genes, "ave_cov"] = genes.loc[contig_genes,:].apply(lambda x: sum(last_contig_cov[(x.start-1):x.end]) / (x.end-x.start+1), axis=1)
                # reset
                last_contig_id  = contig_id
                last_contig_cov = []
            last_contig_cov.append(float(coverage))
        # reached the end of the file --> process last contig
        assert last_contig_id != ""
        contig_genes = genes.contig_id == last_contig_id # genes/proteins of last contig
        # ave. cov. (note: start/end w.r.t. Python indexing)
        genes.loc[contig_genes, "ave_cov"] = genes.loc[contig_genes,:].apply(lambda x: sum(last_contig_cov[(x.start-1):x.end]) / (x.end-x.start+1), axis=1)
    return genes

def mmseqs2_tsv(ifile_path):
    import pandas
    # number of proteins per cluster and tool
    counts = dict()
    with open(ifile_path, "r") as ifile:
        for line in ifile:
            cluster_name, cluster_member = line.strip().split("\t")
            cluster_member_tool = cluster_member.split("__")[0]
            if cluster_name not in counts:
                counts[cluster_name] = dict()
            if cluster_member_tool not in counts[cluster_name]:
                counts[cluster_name][cluster_member_tool] = 0
            counts[cluster_name][cluster_member_tool] += 1

    counts = pandas.DataFrame.from_dict(counts, orient='index')
    return counts

def mmseqs2_summary(counts):
    import pandas
    # number of clusters and cluster members, i.e. proteins, per tool combination
    summary = dict()
    for cluster in counts.index:
        c_comb = ",".join(sorted(counts.columns[ pandas.notnull(counts.loc[cluster,:]) ]))
        c_sum  = counts.loc[cluster,:].sum(skipna=True)
        if c_comb not in summary:
            summary[c_comb] = {"clusters": 0, "members": 0}
        summary[c_comb]["clusters"] += 1
        summary[c_comb]["members"]  += c_sum

    summary = pandas.DataFrame.from_dict(summary, orient='index')
    return summary

def mmseqs2_tool_summary(counts):
    import pandas
    # number of proteins clustered w/ proteins from another tool (per tool)
    summary = pandas.DataFrame(0, index=counts.columns, columns=counts.columns)
    for tool1 in counts.columns:
        for tool2 in counts.columns:
            if tool1 != tool2: # shared w/ other tool
                summary.loc[tool1,tool2] = counts.loc[(pandas.notnull(counts[tool1])) & (pandas.notnull(counts[tool2])), tool1].sum(skipna=True)
            else: # total number of proteins (over all clusters)
                summary.loc[tool1,tool2] = counts[tool1].sum(skipna=True)
    return summary

# metaP

def proc_metap_sec_accs(s, get_tools=False):
    """
    Process secondary accessions from metaP table
    """
    import re
    import pandas
    if pandas.isnull(s) or s == "": # empty
        s = []
    else: # process
        s = s.split(", ") # split IDs into a list
        if get_tools: # extract tool name from IDs
            s = [x.split("__")[0] for x in s if re.search("__", x)]
    return s

def proc_metap_tab(metap_file, mult_tools=False):
    """
    Read in and process a metaP (protein) report
    """
    import pandas
    # read in
    metap = pandas.read_csv(metap_file, sep="\t", header=0)
    if mult_tools:
        # assembly tools from accession IDs; empty if no tool (because not an assembly protein) or accession(s)
        metap["Main Accession Tool"]        = metap["Main Accession"].map(lambda x: ",".join(set(proc_metap_sec_accs(x, True))))
        metap["Secondary Accessions Tools"] = metap["Secondary Accessions"].map(lambda x: ",".join(set(proc_metap_sec_accs(x, True))))
        # rows containing at least one protein ID from the assemblies
        metap["Assembly Proteins"] = (metap["Main Accession Tool"] != "") | (metap["Secondary Accessions Tools"] != "")
        # rows with unique/exclusive assembly proteins
        # unique/exclusive = (same assembly tool for main and all sec. accessions OR no sec. accessions) AND (is assembly prot.)
        metap["Unique"] = ((metap["Main Accession Tool"] == metap["Secondary Accessions Tools"]) | (metap["Secondary Accessions Tools"] == "")) & metap["Assembly Proteins"]
    return metap

def collect_metap_prots(metap):
    """
    Collect all protein accessions from a metaP (protein) report
    """
    import pandas
    # init: main accessions
    metap_prots = set(metap["Main Accession"])
    # add: sec. accessions
    for i in metap.index:
        metap_prots.update(set(proc_metap_sec_accs(metap.loc[i,"Secondary Accessions"], False)))
    return metap_prots