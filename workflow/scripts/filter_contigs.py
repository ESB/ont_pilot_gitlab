#!/usr/bin/env python

from pandas import read_csv
from Bio import SeqIO

cov = read_csv(snakemake.input.cov, sep="\s+", header=None, names=["seq_id", "seq_cov"], index_col=0)

with open(snakemake.input.asm, "r") as ifasta, open(snakemake.output[0], "w") as ofasta:
    for record in SeqIO.parse(ifasta, "fasta"):
        if len(record) >= snakemake.params.minlen and cov.loc[record.id, "seq_cov"] >= snakemake.params.mincov:
            SeqIO.write(record, ofasta, "fasta")
