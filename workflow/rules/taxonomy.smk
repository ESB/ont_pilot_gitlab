# Taxonomic analysis

##################################################
# Kraken2

rule tax_kraken2_contigs:
    input:
        contigs=os.path.join(RESULTS_DIR, "assembly/{rtype}/{tool}/ASSEMBLY.POLISHED.fasta"),
        db=lambda wildcards: os.path.join(DB_DIR, config["kraken2"]["db"][wildcards.db])
    output:
        labels=os.path.join(RESULTS_DIR, "taxonomy/kraken2/{rtype}.{tool}.{db}.labels.txt"),
        report=os.path.join(RESULTS_DIR, "taxonomy/kraken2/{rtype}.{tool}.{db}.report.txt")
    log:
        "logs/tax.kraken2.{rtype}.{tool}.{db}.log"
    wildcard_constraints:
        rtype="|".join(READ_TYPES),
        tool="|".join(ASSEMBLERS),
        db="|".join(config["kraken2"]["db"].keys())
    threads:
        config["kraken2"]["threads"]
    conda:
        "../envs/kraken2.yaml"
    message:
        "Tax. classification w/ Kraken2 ({wildcards.db}, contigs)"
    shell:
        "(date && "
        "kraken2 --db {input.db} --threads {threads} --use-names --report {output.report} --output {output.labels} {input.contigs} && "
        "date) &> {log}"

rule tax_kraken2_sr:
    input:
        r1=os.path.join(RESULTS_DIR, "preproc/{mtype}/sr/R1.proc.fastq.gz"),
        r2=os.path.join(RESULTS_DIR, "preproc/{mtype}/sr/R2.proc.fastq.gz"),
        db=lambda wildcards: os.path.join(DB_DIR, config["kraken2"]["db"][wildcards.db])
    output:
        labels=os.path.join(RESULTS_DIR, "taxonomy/kraken2/{mtype}.sr.{db}.labels.txt"),
        report=os.path.join(RESULTS_DIR, "taxonomy/kraken2/{mtype}.sr.{db}.report.txt")
    log:
        "logs/tax.kraken2.{mtype}.sr.{db}.log"
    wildcard_constraints:
        mtype="|".join(META_TYPES),
        db="|".join(config["kraken2"]["db"].keys())
    threads:
        config["kraken2"]["threads"]
    params:
        params="--gzip-compressed --paired"
    conda:
        "../envs/kraken2.yaml"
    message:
        "Tax. classification w/ Kraken2 ({wildcards.db}, SR)"
    shell:
        "(date && "
        "kraken2 --db {input.db} --threads {threads} --use-names {params.params} --report {output.report} --output {output.labels} {input.r1} {input.r2} && "
        "date) &> {log}"

rule tax_kraken2_lr:
    input:
        lr=os.path.join(RESULTS_DIR, "preproc/metag/lr/lr.proc.fastq.gz"),
        db=lambda wildcards: os.path.join(DB_DIR, config["kraken2"]["db"][wildcards.db])
    output:
        labels=os.path.join(RESULTS_DIR, "taxonomy/kraken2/metag.lr.{db}.labels.txt"),
        report=os.path.join(RESULTS_DIR, "taxonomy/kraken2/metag.lr.{db}.report.txt")
    log:
        "logs/tax.kraken2.metag.lr.{db}.log"
    wildcard_constraints:
        rtype="|".join(READ_TYPES),
        tool="|".join(ASSEMBLERS),
        db="|".join(config["kraken2"]["db"].keys())
    threads:
        config["kraken2"]["threads"]
    conda:
        "../envs/kraken2.yaml"
    message:
        "Tax. classification w/ Kraken2 ({wildcards.db}, LR)"
    shell:
        "(date && "
        "kraken2 --db {input.db} --threads {threads} --use-names --report {output.report} --output {output.labels} {input.lr} && "
        "date) &> {log}"

##################################################
# Kaiju

rule tax_kaiju:
    input:
        faa=os.path.join(RESULTS_DIR, "annotation/prodigal/{rtype}/{tool}/proteins.faa"),
        nodes=lambda wildcards: os.path.join(DB_DIR, config["kaiju"]["db"][wildcards.db], "nodes.dmp"),
        names=lambda wildcards: os.path.join(DB_DIR, config["kaiju"]["db"][wildcards.db], "names.dmp"),
        fmi=lambda wildcards: os.path.join(DB_DIR, config["kaiju"]["db"][wildcards.db], "%s.fmi" % wildcards.db)
    output:
        out=temp(os.path.join(RESULTS_DIR, "taxonomy/kaiju/{rtype}.{tool}.{db}.tsv.tmp")),
        names=os.path.join(RESULTS_DIR, "taxonomy/kaiju/{rtype}.{tool}.{db}.tsv")
    log:
        "logs/tax.kaiju.{rtype}.{tool}.{db}.log"
    wildcard_constraints:
        rtype="|".join(READ_TYPES),
        tool="|".join(ASSEMBLERS),
        db="|".join(config["kaiju"]["db"].keys())
    threads:
        config["kaiju"]["threads"]
    conda:
        "../envs/kaiju.yaml"
    message:
        "Tax. classification w/ Kaiju ({wildcards.db}, proteins)"
    shell:
        "(date && "
        "kaiju -t {input.nodes} -f {input.fmi} -i {input.faa} -o {output.out} -z {threads} -v -p && "
        "kaiju-addTaxonNames -p -t {input.nodes} -n {input.names} -i {output.out} -o {output.names} && "
        "date) &> {log}"

rule tax_kaiju_summary:
    input:
        out=os.path.join(RESULTS_DIR, "taxonomy/kaiju/{rtype}.{tool}.{db}.tsv"),
        nodes=lambda wildcards: os.path.join(DB_DIR, config["kaiju"]["db"][wildcards.db], "nodes.dmp"),
        names=lambda wildcards: os.path.join(DB_DIR, config["kaiju"]["db"][wildcards.db], "names.dmp"),
    output:
        os.path.join(RESULTS_DIR, "taxonomy/kaiju/{rtype}.{tool}.{db}.summary.{rank}.tsv")
    wildcard_constraints:
        rtype="|".join(READ_TYPES),
        tool="|".join(ASSEMBLERS),
        db="|".join(config["kaiju"]["db"].keys()),
        rank="|".join(config["kaiju"]["ranks"])
    threads:
        1
    conda:
        "../envs/kaiju.yaml"
    message:
        "Tax. classification summary w/ Kaiju ({wildcards.db}, {wildcards.rank})"
    shell:
        "kaiju2table -p -t {input.nodes} -n {input.names} -r {wildcards.rank} -o {output} {input.out} -v"
