# Annotation

##################################################
# Prodigal

rule annotation_prodigal:
    input:
        os.path.join(RESULTS_DIR, "assembly/{rtype}/{tool}/ASSEMBLY.POLISHED.fasta")
    output:
        os.path.join(RESULTS_DIR, "annotation/prodigal/{rtype}/{tool}/proteins.faa")
    log:
        "logs/annotation.prodigal.{rtype}.{tool}.log"
    wildcard_constraints:
        rtype="|".join(READ_TYPES),
        tool="|".join(ASSEMBLERS)
    threads: 1
    conda:
        os.path.join(ENV_DIR, "prodigal.yaml")
    message:
        "Annotation: Prodigal: {input}"
    shell:
        "(date && prodigal -a {output} -p meta -i {input} && date) &> {log}"

##################################################
# RGI

# Run RGI: proteins
rule annotation_rgi:
    input:
        faa=os.path.join(RESULTS_DIR, "annotation/prodigal/{rtype}/{tool}/proteins.faa"),
        db=os.path.join(DB_DIR, "rgi/card.json"),
        setup="status/rgi_setup.done" # NOTE: to make sure that the same DB is used for all targets
    output:
        faa=temp(os.path.join(RESULTS_DIR, "annotation/rgi/{rtype}/{tool}/input.faa")),
        txt=os.path.join(RESULTS_DIR, "annotation/rgi/{rtype}/{tool}/rgi.txt")
    log:
        "logs/annotation.rgi.{rtype}.{tool}.log",
    wildcard_constraints:
        rtype="|".join(READ_TYPES),
        tool="|".join(ASSEMBLERS)
    threads:
        config["rgi"]["threads"]
    params:
        alignment_tool="DIAMOND",
        obname=lambda wildcards, output: os.path.splitext(output.txt)[0]
    conda:
        os.path.join(ENV_DIR, "rgi.yaml")
    message:
        "Annotation: RGI: {input}"
    shell:
        "(date && "
        "sed 's/\*$//' {input.faa} > {output.faa} && "
        "rgi database --version --local && "
        # NOTE: https://github.com/arpcard/rgi/issues/93: KeyError: 'snp' --> re-run
        "rgi main --input_sequence {output.faa} --output_file {params.obname} --input_type protein --local -a {params.alignment_tool} --clean -n {threads} || "
        "rgi main --input_sequence {output.faa} --output_file {params.obname} --input_type protein --local -a {params.alignment_tool} --clean -n {threads} && "
        "date) &> {log}"

##################################################
# CRISPR

rule annotation_casc:
    input:
        asm=os.path.join(RESULTS_DIR, "assembly/{rtype}/{tool}/ASSEMBLY.POLISHED.fasta"),
        bin=os.path.join(MOD_DIR, "casc/bin/casc")
    output:
        os.path.join(RESULTS_DIR, "annotation/casc/{rtype}/{tool}/ASSEMBLY.POLISHED.results.txt")
    log:
        "logs/annotation.casc.{rtype}.{tool}.log"
    wildcard_constraints:
        rtype="|".join(READ_TYPES),
        tool="|".join(ASSEMBLERS)
    threads:
        config["casc"]["threads"]
    conda:
        os.path.join(ENV_DIR, "casc.yaml")
    message:
        "Annotation: CASC: {input}"
    shell:
        "(date && "
        "export PATH=$PATH:$(dirname {input.bin}) && "
        "export PERL5LIB=$(dirname $(dirname {input.bin}))/lib && "
        "casc -i {input.asm} -o $(dirname {output}) -n {threads} --conservative && "
        "date) &> {log}"

rule annotation_minced:
    input:
        asm=os.path.join(RESULTS_DIR, "assembly/{rtype}/{tool}/ASSEMBLY.POLISHED.fasta"),
        jar=os.path.join(MOD_DIR, "minced/minced.jar")
    output:
        txt=os.path.join(RESULTS_DIR, "annotation/minced/{rtype}/{tool}/minced.txt"),
        gff=os.path.join(RESULTS_DIR, "annotation/minced/{rtype}/{tool}/minced.gff")
    log:
        "logs/annotation.minced.{rtype}.{tool}.log"
    wildcard_constraints:
        rtype="|".join(READ_TYPES),
        tool="|".join(ASSEMBLERS)
    conda:
        os.path.join(ENV_DIR, "minced.yaml")
    message:
        "Annotation: MinCED: {input}"
    shell:
        "(date && "
        "export PATH=$PATH:$(dirname {input.jar}) && "
        "minced {input.asm} {output.txt} {output.gff} && "
        "date) &> {log}"

##################################################
# Plasflow

rule annotation_plasflow:
    input:
        os.path.join(RESULTS_DIR, "assembly/{rtype}/{tool}/ASSEMBLY.POLISHED.fasta")
    output:
        fasta=temp(os.path.join(RESULTS_DIR, "annotation/plasflow/{rtype}/{tool}/input.fasta")),
        tmp=temp(os.path.join(RESULTS_DIR, "annotation/plasflow/{rtype}/{tool}/plasflow.tsv.tmp")),
        tsv=os.path.join(RESULTS_DIR, "annotation/plasflow/{rtype}/{tool}/plasflow.tsv")
    log:
        "logs/annotation.plasflow.{rtype}.{tool}.log"
    wildcard_constraints:
        rtype="|".join(READ_TYPES),
        tool="|".join(ASSEMBLERS)
    threads: 5
    params:
        script=os.path.join(SRC_DIR, "filter_fasta_by_length.pl"),
        threshold=0.7, # class. prob. threshold
        minlen=1000 # rm contigs with length below this threshold
    conda:
        os.path.join(ENV_DIR, "plasflow.yaml")
    message:
        "Annotation: PlasFlow {input}"
    shell:
        "(date && "
        "{params.script} {params.minlen} {input} > {output.fasta} && "
        "PlasFlow.py --input {output.fasta} --output {output.tmp} --threshold {params.threshold} && "
        "cut -f3,4,6- {output.tmp} > {output.tsv} && "
        "date) &> {log}"

##################################################
# rRNA genes

rule annotation_barrnap:
    input:
        os.path.join(RESULTS_DIR, "assembly/{rtype}/{tool}/ASSEMBLY.POLISHED.fasta")
    output:
        fa=os.path.join(RESULTS_DIR, "annotation/barrnap/{rtype}/{tool}/{kingdom}.fa"),
        gff=os.path.join(RESULTS_DIR, "annotation/barrnap/{rtype}/{tool}/{kingdom}.gff")
    log:
        "logs/annotation.barrnap.{rtype}.{tool}.{kingdom}.log"
    wildcard_constraints:
        rtype="|".join(READ_TYPES),
        tool="|".join(ASSEMBLERS),
        kingdom="|".join(config["barrnap"]["kingdom"])
    threads:
        config["barrnap"]["threads"]
    conda:
        os.path.join(ENV_DIR, "barrnap.yaml")
    message:
        "Annotation: barrnap: {input}"
    shell:
        "(date && barrnap --threads {threads} --kingdom {wildcards.kingdom} --outseq {output.fa} < {input} > {output.gff} && date) &> {log}"

##################################################
# HMM

rule annotation_hmm_kegg:
    input:
        faa=os.path.join(RESULTS_DIR, "annotation/prodigal/{rtype}/{tool}/proteins.faa"),
        hmm=[os.path.join(DB_DIR, config["hmm"]["kegg"])] if config["hmm"]["kegg"] else []
    output:
        os.path.join(RESULTS_DIR, "annotation/hmm/kegg/{rtype}/{tool}/hmm.tblout")
    log:
        "logs/annotation.hmm.kegg.{rtype}.{tool}.log"
    wildcard_constraints:
        rtype="|".join(READ_TYPES),
        tool="|".join(ASSEMBLERS)
    threads:
        config["hmm"]["threads"]
    conda:
        os.path.join(ENV_DIR, "hmmer.yaml")
    message:
        "Annotation: HMM search (KEGG): {input}"
    shell:
        "(date && (hmmsearch --cpu {threads} --noali --notextw --tblout {output} {input.hmm} {input.faa}) > /dev/null && date) &> {log}"
