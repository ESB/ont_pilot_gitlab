# Analysis

localrules: analysis_rename_proteins

rule analysis_rename_proteins:
    input:
        os.path.join(RESULTS_DIR, "annotation/prodigal/{rtype}/{tool}/proteins.faa")
    output:
        temp(os.path.join(RESULTS_DIR, "annotation/prodigal/{rtype}/{tool}/proteins.renamed.faa"))
    wildcard_constraints:
        rtype="|".join(READ_TYPES),
        tool="|".join(ASSEMBLERS)
    threads: 1
    message:
        "Analysis: Rename proteins: {input}"
    shell:
        "sed 's/^>/>{wildcards.rtype}__{wildcards.tool}__/' {input} > {output}"

##################################################
# QUAST

rule analysis_quast:
    input:
        os.path.join(RESULTS_DIR, "assembly/{rtype}/{tool}/ASSEMBLY.POLISHED.fasta")
    output:
        os.path.join(RESULTS_DIR, "analysis/quast/{rtype}/{tool}/report.tsv")
    log:
        "logs/analysis.quast.{rtype}.{tool}.log"
    wildcard_constraints:
        rtype="|".join(READ_TYPES),
        tool="|".join(ASSEMBLERS)
    threads:
        config["quast"]["threads"]
    params:
        params="--max-ref-number 0 --min-contig 0 --contig-thresholds 0,1000,2000,5000"
    conda:
        os.path.join(ENV_DIR, "quast.yaml")
    message:
        "Analysis: metaQUAST: {input}"
    shell:
        "(date && "
        "metaquast.py {params.params} --threads {threads} {input} -o $(dirname {output}) && "
        "date) &> {log}"

##################################################
# Mash

#########################
# Reads
rule analysis_mash_sketch_sr:
    input:
        r1=os.path.join(RESULTS_DIR, "preproc/{mtype}/sr/R1.proc.fastq.gz"),
        r2=os.path.join(RESULTS_DIR, "preproc/{mtype}/sr/R2.proc.fastq.gz")
    output:
        temp(os.path.join(RESULTS_DIR, "preproc/{mtype}/sr/fastp.msh"))
    log:
        "logs/analysis.mash.sketch.{mtype}.sr.log"
    wildcard_constraints:
        mtype="metag|metat"
    threads: 1
    params:
        k=31,
        s=100000
    conda:
        os.path.join(ENV_DIR, "mash.yaml")
    message:
        "Analysis: Mash: sketch: {input}"
    shell:
        "(date && ofile={output} && mash sketch -S 42 -r -m 2 -k {params.k} -s {params.s} -o ${{ofile%.*}} {input} && date) &> {log}"

rule analysis_mash_sketch_lr:
    input:
        os.path.join(RESULTS_DIR, "preproc/metag/lr/lr.proc.fastq.gz")
    output:
        temp(os.path.join(RESULTS_DIR, "preproc/metag/lr/fastp.msh"))
    log:
        "logs/analysis.mash.sketch.metag.lr.log"
    threads: 1
    params:
        k=31,
        s=100000
    conda:
        os.path.join(ENV_DIR, "mash.yaml")
    message:
        "Analysis: Mash: sketch: {input}"
    shell:
        "(date && ofile={output} && mash sketch -S 42 -r -m 2 -k {params.k} -s {params.s} -o ${{ofile%.*}} {input} && date) &> {log}"

rule analysis_mash_reads:
    input:
        sr=expand(os.path.join(RESULTS_DIR, "preproc/{mtype}/sr/fastp.msh"), mtype=META_TYPES),
        lr=os.path.join(RESULTS_DIR, "preproc/metag/lr/fastp.msh")
    output:
        sketch=os.path.join(RESULTS_DIR, "analysis/mash/reads.msh"),
        dist=os.path.join(RESULTS_DIR, "analysis/mash/reads.dist")
    log:
        "logs/analysis.mash.dist.reads.log"
    threads:
        config["mash"]["threads"]
    conda:
        os.path.join(ENV_DIR, "mash.yaml")
    message:
        "Analysis: Mash: {input}"
    shell:
        "(date && "
        "ofile={output.sketch} && mash paste ${{ofile%.*}} {input} && "
        "mash dist -t -p {threads} {output.sketch} {output.sketch} > {output.dist} && "
        "date) &> {log}"

#########################
# Assemblies
rule analysis_mash_sketch_asm:
    input:
        os.path.join(RESULTS_DIR, "assembly/{rtype}/{tool}/ASSEMBLY.POLISHED.fasta")
    output:
        temp(os.path.join(RESULTS_DIR, "assembly/{rtype}/{tool}/ASSEMBLY.POLISHED.msh"))
    log:
        "logs/mash.sketch.{rtype}.{tool}.log"
    threads: 1
    params:
        k=31,
        s=100000
    conda:
        os.path.join(ENV_DIR, "mash.yaml")
    message:
        "Analysis: Mash: sketch: {input}"
    shell:
        "(date && ofile={output} && mash sketch -S 42 -k {params.k} -s {params.s} -o ${{ofile%.*}} {input} && date) &> {log}"
    
rule analysis_mash_asm:
    input:
        expand(
            os.path.join(RESULTS_DIR, "assembly/{rtype_tool}/ASSEMBLY.POLISHED.msh"),
            rtype_tool=["%s/%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS],
        )
    output:
        sketch=os.path.join(RESULTS_DIR, "analysis/mash/contigs.msh"),
        dist=os.path.join(RESULTS_DIR, "analysis/mash/contigs.dist")
    log:
        "logs/mash.dist.contigs.log"
    threads:
        config["mash"]["threads"]
    conda:
        os.path.join(ENV_DIR, "mash.yaml")
    message:
        "Analysis: Mash: {input}"
    shell:
        "(date && "
        "ofile={output.sketch} && mash paste ${{ofile%.*}} {input} && "
        "mash dist -t -p {threads} {output.sketch} {output.sketch} > {output.dist} && "
        "date) &> {log}"

##################################################
# MashMap

# one-to-one mappings between two assemblies
# results are filtered to get best hit per query and reference, splitting is not allowed
rule analysis_mashmap_one2one:
    input:
        asm1=os.path.join(RESULTS_DIR, "assembly/{rtype1}/{tool1}/ASSEMBLY.POLISHED.fasta"),
        asm2=os.path.join(RESULTS_DIR, "assembly/{rtype2}/{tool2}/ASSEMBLY.POLISHED.fasta")
    output:
        out=os.path.join(RESULTS_DIR, "analysis/mashmap/{rtype1}_{tool1}__{rtype2}_{tool2}.one2one.out")
    log:
        "logs/analysis.mashmap.one2one2.{rtype1}.{tool1}.{rtype2}.{tool2}.log"
    wildcard_constraints:
        rtype1="|".join(READ_TYPES),
        rtype2="|".join(READ_TYPES),
        tool1="|".join(ASSEMBLERS),
        tool2="|".join(ASSEMBLERS)
    threads:
        config["mashmap"]["threads"]
    params:
        params="-s 1000 --pi 95 -f one-to-one --noSplit"
    conda:
        os.path.join(ENV_DIR, "mashmap.yaml")
    message:
        "Analysis: MashMap: one-to-one: {input}"
    shell:
        "(date && mashmap -r {input.asm1} -q {input.asm2} {params.params} -t {threads} -o {output.out} && date) &> {log}"

##################################################
# FastANI

rule analysis_fastani_one2one:
    input:
        expand(
            os.path.join(RESULTS_DIR, "assembly/{rtype_tool}/ASSEMBLY.POLISHED.fasta"),
            rtype_tool=["%s/%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS],
        )
    output:
        out=os.path.join(RESULTS_DIR, "analysis/fastani/many2many.out"),
        qlrl=temp(os.path.join(RESULTS_DIR, "analysis/fastani/input.txt"))
    log:
        "logs/analysis.fastani.many2many.log"
    threads:
        config["fastani"]["threads"]
    params:
        params="-s 1000 -pi 95 -f one-to-one --noSplit"
    conda:
        os.path.join(ENV_DIR, "fastani.yaml")
    message:
        "Analysis: fastani: one-to-one: {input}"
    shell:
        "(date && "
        "echo \"{input}\" | sed 's/\s\+/\\n/g' > {output.qlrl} && "
        "fastANI --ql {output.qlrl} --rl {output.qlrl} -o {output.out} --threads {threads} && "
        "date) &> {log}"

##################################################
# MUMMER

rule analysis_mummer_dnadiff:
    input:
        asm1=os.path.join(RESULTS_DIR, "assembly/{rtype1}/{tool1}/ASSEMBLY.POLISHED.fasta"),
        asm2=os.path.join(RESULTS_DIR, "assembly/{rtype2}/{tool2}/ASSEMBLY.POLISHED.fasta")
    output:
        out=os.path.join(RESULTS_DIR, "analysis/mummer/{rtype1}_{tool1}__{rtype2}_{tool2}.dnadiff.report"),
    log:
        "logs/analysis.mummer.dnadiff.{rtype1}.{tool1}.{rtype2}.{tool2}.log"
    wildcard_constraints:
        rtype1="|".join(READ_TYPES),
        rtype2="|".join(READ_TYPES),
        tool1="|".join(ASSEMBLERS),
        tool2="|".join(ASSEMBLERS)
    threads: 1
    conda:
        os.path.join(ENV_DIR, "mummer.yaml")
    message:
        "Analysis: MUMMER: dnadiff: {input}"
    shell:
        "(date && "
        "pout=$(dirname {output.out})/$(basename -s '.report' {output.out}) && "
        "dnadiff -p ${{pout}} {input.asm1} {input.asm2} && "
        "date) &> {log}"

##################################################
# CD-HIT
# https://github.com/weizhongli/cdhit/wiki/3.-User's-Guide#CDHIT2D

rule analysis_cdhit:
    input:
        faa1=os.path.join(RESULTS_DIR, "annotation/prodigal/{rtype1}/{tool1}/proteins.renamed.faa"),
        faa2=os.path.join(RESULTS_DIR, "annotation/prodigal/{rtype2}/{tool2}/proteins.renamed.faa")
    output:
        faa=os.path.join(RESULTS_DIR, "analysis/cdhit/{rtype1}_{tool1}__{rtype2}_{tool2}.faa")
    log:
        "logs/analysis.cdhit.{rtype1}.{tool1}.{rtype2}.{tool2}.log"
    wildcard_constraints:
        rtype1="|".join(READ_TYPES),
        rtype2="|".join(READ_TYPES),
        tool1="|".join(ASSEMBLERS),
        tool2="|".join(ASSEMBLERS)
    threads:
        config["cdhit"]["threads"]
    params:
        params="-c 0.9 -n 5 -d 0 -M 16000 -s2 0.9"
    conda:
        os.path.join(ENV_DIR, "cdhit.yaml")
    message:
        "Analysis: CD-HIT: {input}"
    shell:
        "(date && "
        "cd-hit-2d -i {input.faa1} -i2 {input.faa2} -o {output.faa} {params.params} -T {threads} && "
        "date) &> {log}"

##################################################
# DIAMOND

rule analysis_diamond:
    input:
        faa1=os.path.join(RESULTS_DIR, "annotation/prodigal/{rtype1}/{tool1}/proteins.faa"),
        faa2=os.path.join(RESULTS_DIR, "annotation/prodigal/{rtype2}/{tool2}/proteins.faa")
    output:
        db=temp(os.path.join(RESULTS_DIR, "analysis/diamond/{rtype1}_{tool1}__{rtype2}_{tool2}.dmnd")),
        daa=os.path.join(RESULTS_DIR, "analysis/diamond/{rtype1}_{tool1}__{rtype2}_{tool2}.daa"),
        tsv=os.path.join(RESULTS_DIR, "analysis/diamond/{rtype1}_{tool1}__{rtype2}_{tool2}.tsv")
    log:
        "logs/analysis.diamond.{rtype1}.{tool1}.{rtype2}.{tool2}.log"
    wildcard_constraints:
        rtype1="|".join(READ_TYPES),
        rtype2="|".join(READ_TYPES),
        tool1="|".join(ASSEMBLERS),
        tool2="|".join(ASSEMBLERS)
    threads:
        config["diamond"]["threads"]
    params:
        blastp="--id 90 --query-cover 90 --subject-cover 90 --more-sensitive",
        outfmt="6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qlen slen"
    conda:
        os.path.join(ENV_DIR, "diamond.yaml")
    message:
        "Analysis: DIAMOND search: {input}"
    shell:
        "(date && "
        "db={output.db} && "
        "daa={output.daa} && "
        "diamond makedb --in {input.faa1} --db ${{db%.*}} --threads {threads} && "
        "diamond blastp -q {input.faa2} --db ${{db%.*}} --out ${{daa}} --threads {threads} --outfmt 100 {params.blastp} && "
        "diamond view --daa ${{daa%.*}} --max-target-seqs 1 --threads {threads} --outfmt {params.outfmt} --out {output.tsv} && "
        "date) &> {log}"

# Blog post: http://www.opiniomics.org/a-simple-test-for-uncorrected-insertions-and-deletions-indels-in-bacterial-genomes/
rule analysis_diamond_db:
    input: 
        faa=os.path.join(RESULTS_DIR, "annotation/prodigal/{rtype}/{tool}/proteins.faa"),
        db=os.path.join(DB_DIR, config["diamond"]["db"])
    output:
        daa=os.path.join(RESULTS_DIR, "analysis/diamond/{rtype}_{tool}.daa"),
        tsv=os.path.join(RESULTS_DIR, "analysis/diamond/{rtype}_{tool}.tsv")
    log:
        "logs/analysis.diamond.db.{rtype}.{tool}.log",
    wildcard_constraints:
        rtype="|".join(READ_TYPES),
        tool="|".join(ASSEMBLERS)
    threads:
        config["diamond"]["threads"]
    params:
        outfmt="6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qlen slen"
    conda:
        os.path.join(ENV_DIR, "diamond.yaml")
    message:
        "Analysis: DIAMOND search: {input}"
    shell: 
        "(date && "
        "daa={output.daa} && "
        "diamond blastp -q {input.faa} --db {input.db} --out {output.daa} -p {threads} --outfmt 100 && "
        "diamond view --daa ${{daa%.*}} --max-target-seqs 1 -p {threads} --outfmt {params.outfmt} --out {output.tsv} && "
        "date) &> {log}"

##################################################
# MMSEQS2

rule analysis_mmseqs2:
    input:
        expand(
            os.path.join(RESULTS_DIR, "annotation/prodigal/{rtype_tool}/proteins.faa"),
            rtype_tool=["%s/%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS]
        )
    output:
        tsv=os.path.join(RESULTS_DIR, "analysis/mmseqs2/clusters.tsv"),
        faa=temp(os.path.join(RESULTS_DIR, "analysis/mmseqs2/proteins.faa")) if "metap" not in config["data"] else os.path.join(RESULTS_DIR, "analysis/mmseqs2/proteins.faa"),
        tmpdir=temp(directory(os.path.join(RESULTS_DIR, "analysis/mmseqs2/tmp")))
    log:
        "logs/analysis.mmseqs2.log"
    threads:
        config["mmseqs2"]["threads"]
    params:
        # basenames (w/ extension) for database and clustering files
        db=os.path.join(RESULTS_DIR, "analysis/mmseqs2/mmseqs2DB"),
        cl=os.path.join(RESULTS_DIR, "analysis/mmseqs2/mmseqs2CLUST")
    conda:
        os.path.join(ENV_DIR, "mmseqs2.yaml")
    message:
        "Analysis: MMSeqs2 clustering: {input}"
    shell:
        "(date && "
        # put all proteins together
        "for f in {input}; do sed 's/^>/>'$(basename $(dirname ${{f}}))'__/' ${{f}}; done > {output.faa} && "
        # DB, cluster, TSV output
        "mmseqs createdb --dbtype 1 -v 3 {output.faa} {params.db} && "
        "mmseqs linclust --threads {threads} {params.db} {params.cl} {output.tmpdir} && "
        "mmseqs createtsv {params.db} {params.db} {params.cl} {output.tsv} && "
        "date) &> {log}"

# FAA file of cluster representatives
rule analysis_mmseqs2_cluster_faa:
    input:
        faa=os.path.join(RESULTS_DIR, "analysis/mmseqs2/proteins.faa"),
        tsv=os.path.join(RESULTS_DIR, "analysis/mmseqs2/clusters.tsv")
    output:
        os.path.join(RESULTS_DIR, "analysis/mmseqs2/clusters.faa")
    conda:
        os.path.join(ENV_DIR, "biopython.yaml")
    message:
        "Analysis: MMSeqs2 clustering FAA: {input}"
    script:
        os.path.join(SRC_DIR, "mmseqs2_cluster_faa.py")

##################################################
# COVERAGE

rule analysis_genomecov_pergene:
    input:
        faa=os.path.join(RESULTS_DIR, "annotation/prodigal/{rtype}/{tool}/proteins.faa"),
        cov=os.path.join(RESULTS_DIR, "mapping/{mtype}/{rtype}/{tool}/ASSEMBLY.POLISHED.sr.cov.perbase")
    output:
        os.path.join(RESULTS_DIR, "mapping/{mtype}/{rtype}/{tool}/ASSEMBLY.POLISHED.sr.cov.pergene")
    wildcard_constraints:
        mtype="|".join(META_TYPES),
        rtype="|".join(READ_TYPES),
        tool="|".join(ASSEMBLERS)
    threads: 1
    message:
        "Analysis: coverage per gene: {input}"
    run:
        from scripts.utils import parse_prodigal_faa_headers, ave_gene_cov
        genes = parse_prodigal_faa_headers(input.faa)
        genes = ave_gene_cov(input.cov, genes)
        genes.to_csv(output[0], sep="\t", header=True, index=False, index_label=False)

# contig coverage segmentation
rule analysis_genomecov_segmentation:
    input:
        cov=os.path.join(RESULTS_DIR, "mapping/metag/{rtype}/{tool}/ASSEMBLY.POLISHED.sr.cov.perbase"),
        installed=os.path.join(MOD_DIR, "segclust2d.installed")
    output:
        seg=os.path.join(RESULTS_DIR, "mapping/metag/{rtype}/{tool}/ASSEMBLY.POLISHED.sr.cov.seg.tsv"),
        sum=os.path.join(RESULTS_DIR, "mapping/metag/{rtype}/{tool}/ASSEMBLY.POLISHED.sr.cov.segsum.tsv"),
    log:
        out="logs/analysis.covseg.{rtype}.{tool}.log"
    wildcard_constraints:
        rtype="|".join(READ_TYPES),
        tool="|".join(ASSEMBLERS)
    params:
        lmin=10000,
    threads: 10
    conda:
        os.path.join(ENV_DIR, "segclust2d.yaml")
    message:
        "Analysis: contig coverage multi-modality: {input}"
    script:
        os.path.join(SRC_DIR, "cov_multimod_segclust2d.R")
