# Assembly

##################################################
# Long reads

rule assembly_lr_flye:
    input:
        os.path.join(RESULTS_DIR, "preproc/metag/lr/lr.proc.fastq.gz")
    output:
        os.path.join(RESULTS_DIR, "assembly/lr/flye/ASSEMBLY.fasta")
    threads:
        config["flye"]["threads"]
    log:
        "logs/assembly.lr.flye.log"
    conda:
        os.path.join(ENV_DIR, "flye.yaml")
    message:
        "Assembly: long reads: Flye"
    shell:
        "(date && "
        "flye --nano-raw {input} --meta --out-dir $(dirname {output}) --threads {threads} && "
        "cd $(dirname {output}) && ln -s assembly.fasta $(basename {output}) && "
        "date) &> {log}"

rule assembly_lr_raven:
    input:
        os.path.join(RESULTS_DIR, "preproc/metag/lr/lr.proc.fastq.gz")
    output:
        os.path.join(RESULTS_DIR, "assembly/lr/raven/ASSEMBLY.fasta")
    threads:
        config["raven"]["threads"]
    log:
        "logs/assembly.lr.raven.log"
    conda:
        os.path.join(ENV_DIR, "raven.yaml")
    message:
        "Assembly: long reads: Raven"
    shell:
        "(date && "
        "raven -t{threads} {input} > {output} && "
        "date) &> {log}"

rule assembly_lr_canu:
    input:
        os.path.join(RESULTS_DIR, "preproc/metag/lr/lr.proc.fastq.gz")
    output:
        os.path.join(RESULTS_DIR, "assembly/lr/canu/ASSEMBLY.fasta")
    log:
        "logs/assembly.lr.canu.log"
    threads:
        config["canu"]["threads"]
    conda:
        os.path.join(ENV_DIR, "canu.yaml")
    message:
        "Assembly: long reads: canu"
    shell:
        "(date && "
        "canu -nanopore-raw {input} -p \"assembly\" -d $(dirname {output}) genomeSize={config[canu][genome_size]} "
        "useGrid=false minThreads={threads} maxThreads={threads} minInputCoverage=8 stopOnLowCoverage=8 && "
        "cd $(dirname {output}) && ln -s assembly.contigs.fasta $(basename {output}) && "
        "date) &> {log}"

##################################################
# Short reads

rule assembly_sr_megahit:
    input: 
        r1=os.path.join(RESULTS_DIR, "preproc/metag/sr/R1.proc.fastq.gz"),
        r2=os.path.join(RESULTS_DIR, "preproc/metag/sr/R2.proc.fastq.gz")
    output:
        os.path.join(RESULTS_DIR, "assembly/sr/megahit/ASSEMBLY.fasta")
    log:
        "logs/assembly.sr.megahit.log",
    threads:
        config["megahit"]["threads"]
    conda:
        os.path.join(ENV_DIR, "megahit.yaml")
    message:
        "Assembly: short reads: MEGAHIT"
    shell:
        "(date && megahit -1 {input.r1} -2 {input.r2} -t {threads} -o $(dirname {output})/tmp && "
        "cd $(dirname {output}) && "
        "rsync -avP tmp/ . && "
        "ln -sf final.contigs.fa $(basename {output}) && "
        "rm -rf tmp/ && "
        "date) &> {log}"

rule assembly_sr_metaspades:
    input:
        r1=os.path.join(RESULTS_DIR, "preproc/metag/sr/R1.proc.fastq.gz"),
        r2=os.path.join(RESULTS_DIR, "preproc/metag/sr/R2.proc.fastq.gz")
    output:
        os.path.join(RESULTS_DIR, "assembly/sr/metaspades/ASSEMBLY.fasta")
    log:
        "logs/assembly.sr.metaspades.log"
    threads:
        config["metaspades"]["threads"]
    conda:
        os.path.join(ENV_DIR, "spades.yaml")
    message:
        "Assembly: short reads: MetaSPAdes"
    shell:
        "(date && "
        "metaspades.py -k 21,33,55,77 -t {threads} -1 {input.r1} -2 {input.r2} -o $(dirname {output}) && "
        "cd $(dirname {output}) && ln -sf contigs.fasta $(basename {output}) && "
        "date) &> {log}"

##################################################
# Hybrid

rule assembly_hy_metaspades:
    input: 
        lr=os.path.join(RESULTS_DIR, "preproc/metag/lr/lr.proc.fastq.gz"),
        r1=os.path.join(RESULTS_DIR, "preproc/metag/sr/R1.proc.fastq.gz"),
        r2=os.path.join(RESULTS_DIR, "preproc/metag/sr/R2.proc.fastq.gz")
    output: 
        os.path.join(RESULTS_DIR, "assembly/hy/metaspadeshybrid/ASSEMBLY.fasta")
    log:
        "logs/assembly.hy.metaspadeshybrid.log"
    threads:
        config["metaspades"]["threads"]
    conda:
        os.path.join(ENV_DIR, "spades.yaml")
    message:
        "Assembly: hybrid: MetaSPAdes"
    shell:
        "(date && "
        "spades.py --meta -k 21,33,55,77 -t {threads} -1 {input.r1} -2 {input.r2} --nanopore {input.lr} -o $(dirname {output}) && "
        "cd $(dirname {output}) && ln -sf scaffolds.fasta $(basename {output}) && "
        "date) &> {log}"

rule assembly_hy_operams:
    input: 
        lr=os.path.join(RESULTS_DIR, "preproc/metag/lr/lr.proc.fastq.gz"),
        r1=os.path.join(RESULTS_DIR, "preproc/metag/sr/R1.proc.fastq.gz"),
        r2=os.path.join(RESULTS_DIR, "preproc/metag/sr/R2.proc.fastq.gz"),
        asm=os.path.join(RESULTS_DIR, "assembly/sr/{asm}/ASSEMBLY.fasta"),
        bin=os.path.join(MOD_DIR, "operams/OPERA-MS.pl"),
        install=os.path.join(MOD_DIR, "operams/installation.done")
    output:
        asm=os.path.join(RESULTS_DIR, "assembly/hy/operams{asm}/ASSEMBLY.fasta"),
        lr=temp(os.path.join(RESULTS_DIR, "assembly/hy/operams{asm}/lr.fastq"))
    log:
        "logs/assembly.hy.operams.{asm}.log"
    wildcard_constraints:
        asm="|".join(config["assemblers"]["sr"])
    threads:
        config["operams"]["threads"]
    conda:
        os.path.join(ENV_DIR, "operams.yaml")
    message:
        "Assembly: hybrid: OPERA-MS using contigs from {wildcards.asm}"
    shell:
       "(date && "
       "zcat {input.lr} > {output.lr} && "
       "perl {input.bin} --short-read1 {input.r1} --short-read2 {input.r2} --long-read {output.lr} --contig-file {input.asm} "
       "--out-dir $(dirname {output.asm}) --long-read-mapper minimap2 --num-processors {threads} && "
       "cd $(dirname {output.asm}) && "
       "ln -sf contigs.polished.fasta $(basename {output.asm}) && "
       "tar -czvf intermediate_files.tar.gz intermediate_files/ && rm -rf intermediate_files/ && "
       "date) &> {log}"

##################################################
# Polishing

localrules: link_polished_assembly_sr, link_polished_assembly_lr, link_polished_assembly_hy

rule link_polished_assembly_sr:
    input:
        os.path.join(RESULTS_DIR, "assembly/sr/{tool}/ASSEMBLY.fasta")
    output:
        os.path.join(RESULTS_DIR, "assembly/sr/{tool}/ASSEMBLY.POLISHED.fasta")
    wildcard_constraints:
        tool="|".join(config["assemblers"]["sr"]),
    shell:
        "cd $(dirname {output}) && ln -s $(basename {input}) $(basename {output})"

rule link_polished_assembly_lr:
    input:
        os.path.join(RESULTS_DIR, "assembly/lr/{tool}/POLISHING_raconLR1_raconSR4_medakaLR1/ASSEMBLY.fasta")
    output:
        os.path.join(RESULTS_DIR, "assembly/lr/{tool}/ASSEMBLY.POLISHED.fasta")
    wildcard_constraints:
        tool="|".join(config["assemblers"]["lr"]),
    shell:
        "cd $(dirname {output}) && ln -s $(basename $(dirname {input}))/$(basename {input}) $(basename {output})"

rule link_polished_assembly_hy:
    input:
        os.path.join(RESULTS_DIR, "assembly/hy/{tool}/POLISHING_raconSR5/ASSEMBLY.fasta")
    output:
        os.path.join(RESULTS_DIR, "assembly/hy/{tool}/ASSEMBLY.POLISHED.fasta")
    wildcard_constraints:
        tool="|".join(config["assemblers"]["hy"]),
    shell:
        "cd $(dirname {output}) && ln -s $(basename $(dirname {input}))/$(basename {input}) $(basename {output})"

rule polishing_racon_lr:
    input:
        lr=os.path.join(RESULTS_DIR, "preproc/metag/lr/lr.proc.fastq.gz"),
        asm=lambda wildcards:
            os.path.join(RESULTS_DIR, "assembly/lr/{tool}/ASSEMBLY.fasta") if wildcards.times == "1" else
            os.path.join(RESULTS_DIR, "assembly/lr/{{tool}}/{{ptype}}_raconLR{times}/ASSEMBLY.fasta".format(
                times=str(int(wildcards.times)-1)
            )),
    output:
        fasta=os.path.join(RESULTS_DIR, "assembly/lr/{tool}/{ptype}_raconLR{times}/ASSEMBLY.fasta"),
        bam=temp(os.path.join(RESULTS_DIR, "assembly/lr/{tool}/{ptype}_raconLR{times}/input.bam")),
        sam=temp(os.path.join(RESULTS_DIR, "assembly/lr/{tool}/{ptype}_raconLR{times}/input.sam")),
        asm=temp(os.path.join(RESULTS_DIR, "assembly/lr/{tool}/{ptype}_raconLR{times}/input.fasta"))
    log:
        "logs/polishing.lr.{tool}.{ptype}_raconLR{times}.log"
    wildcard_constraints:
        tool="|".join(config["assemblers"]["lr"]),
        ptype="POLISHING.*",
        times="[0-9]+"
    threads:
        config["racon"]["threads"]
    params:
        idx_prefix=lambda wildcards, output, input: os.path.splitext(input.asm)[0],
        bam_prefix=lambda wildcards, output: os.path.splitext(output.bam)[0],
        chunk_size=config["samtools"]["sort"]["chunk_size_bigmem"]
    conda:
        os.path.join(ENV_DIR, "racon.yaml")
    message:
        "Assembly polishing: Racon w/ LR ({wildcards.times}. time)"
    shell:
        "(date && "
        # FASTA index
        "bwa index {input.asm} -p {params.idx_prefix} && "
        # mapping
        "bwa mem -x ont2d -t {threads} {params.idx_prefix} {input.lr} | "
        "samtools view -@ {threads} -SbT {input.asm} | "
        "samtools sort -@ {threads} -m {params.chunk_size} -T {params.bam_prefix} > {output.bam} && "
        "samtools view -@ {threads} -h -o {output.sam} {output.bam} && "
        # FASTA copy
        "cp {input.asm} {output.asm} && "
        # polishing
        "racon --include-unpolished -t {threads} {input.lr} {output.sam} {output.asm} > {output.fasta} && "
        "date) &> {log}"

rule polishing_racon_sr:
    input:
        r1=os.path.join(RESULTS_DIR, "preproc/metag/sr/R1.proc.fastq.gz"),
        r2=os.path.join(RESULTS_DIR, "preproc/metag/sr/R2.proc.fastq.gz"),
        asm=lambda wildcards:
            os.path.join(RESULTS_DIR, "assembly/{rtype}/{tool}/ASSEMBLY.fasta") if wildcards.times == "1" and wildcards.rtype == "hy" else
            os.path.join(RESULTS_DIR, "assembly/{{rtype}}/{{tool}}/{ptype}_raconLR1/ASSEMBLY.fasta".format(
                ptype="|".join(wildcards.ptype.split("_")[:-1])
            )) if wildcards.times == "1" and wildcards.rtype == "lr" else
            os.path.join(RESULTS_DIR, "assembly/{{rtype}}/{{tool}}/{{ptype}}_raconSR{times}/ASSEMBLY.fasta".format(
                times=str(int(wildcards.times)-1)
            )),
    output:
        fasta=os.path.join(RESULTS_DIR, "assembly/{rtype}/{tool}/{ptype}_raconSR{times}/ASSEMBLY.fasta"),
        bam=temp(os.path.join(RESULTS_DIR, "assembly/{rtype}/{tool}/{ptype}_raconSR{times}/input.bam")),
        sam=temp(os.path.join(RESULTS_DIR, "assembly/{rtype}/{tool}/{ptype}_raconSR{times}/input.sam")),
        asm=temp(os.path.join(RESULTS_DIR, "assembly/{rtype}/{tool}/{ptype}_raconSR{times}/input.fasta")),
        fastq=temp(os.path.join(RESULTS_DIR, "assembly/{rtype}/{tool}/{ptype}_raconSR{times}/input.fastq")),
    log:
        "logs/polishing.{rtype}.{tool}.{ptype}_raconSR{times}.log"
    wildcard_constraints:
        rtype="|".join(["lr", "hy"]),
        tool="|".join(config["assemblers"]["lr"] + config["assemblers"]["hy"]),
        ptype="POLISHING.*",
        times="[0-9]+"
    threads:
        config["racon"]["threads"]
    params:
        idx_prefix=lambda wildcards, output, input: os.path.splitext(input.asm)[0],
        bam_prefix=lambda wildcards, output: os.path.splitext(output.bam)[0],
        chunk_size=config["samtools"]["sort"]["chunk_size_bigmem"]
    conda:
        os.path.join(ENV_DIR, "racon.yaml")
    message:
        "Assembly polishing: Racon w/ SR ({wildcards.times}. time)"
    shell:
        "(date && "
        # FASTA index
        "bwa index {input.asm} -p {params.idx_prefix} && "
        # FASTQ -> FASTA
        "zcat {input.r1} | awk '{{print (NR%4 == 1) ? \"@1_\" ++i : $0}}'  > {output.fastq} && "
        "zcat {input.r2} | awk '{{print (NR%4 == 1) ? \"@2_\" ++i : $0}}' >> {output.fastq} && "
        # mapping
        "bwa mem -t {threads} {params.idx_prefix} {output.fastq} | "
        "samtools view -@ {threads} -SbT {input.asm} | "
        "samtools sort -@ {threads} -m {params.chunk_size} -T {params.bam_prefix} > {output.bam} && "
        "samtools view -@ {threads} -h -o {output.sam} {output.bam} && "
        # FASTA copy
        "cp {input.asm} {output.asm} && "
        # polishing
        "racon --include-unpolished -t {threads} {output.fastq} {output.sam} {output.asm} > {output.fasta} && "
        "date) &> {log}"

rule polishing_medaka_lr:
    input:
        lr=os.path.join(RESULTS_DIR, "preproc/metag/lr/lr.proc.fastq.gz"),
        asm=lambda wildcards:
            os.path.join(RESULTS_DIR, "assembly/lr/{tool}/POLISHING_raconLR1_raconSR4/ASSEMBLY.fasta") if wildcards.times == "1" else
            os.path.join(RESULTS_DIR, "assembly/lr/{{tool}}/{{ptype}}_medakaLR{times}/ASSEMBLY.fasta".format(
                times=str(int(wildcards.times)-1)
            )),
    output:
        os.path.join(RESULTS_DIR, "assembly/lr/{tool}/{ptype}_medakaLR{times}/ASSEMBLY.fasta")
    log:
        "logs/polishing.lr.{tool}.{ptype}_medakaLR{times}.log"
    wildcard_constraints:
        tool="|".join(config["assemblers"]["lr"]),
        ptype="POLISHING.*",
        times="[0-9]+"
    threads:
        config["medaka"]["threads"]
    params:
        model=config["medaka"]["model"]
    conda:
        os.path.join(ENV_DIR, "medaka.yaml")
    message:
        "Assembly polishing: Medaka w/ LR ({wildcards.times}. time)"
    shell:
        "(date && "
        "medaka_consensus -i {input.lr} -d {input.asm} -o $(dirname {output}) -t {threads} -m {params.model} && "
        "cd $(dirname {output}) && ln -sf consensus.fasta $(basename {output}) && "
        "date) &> {log}"

#################################################
# Contig filtering

# rule filter_contigs:
#     input:
#         asm=os.path.join(RESULTS_DIR, "assembly/{rtype}/{tool}/ASSEMBLY.fasta"),
#         cov=os.path.join(RESULTS_DIR, "mapping/metag/{rtype}/{tool}/ASSEMBLY.{rtype}.cov.ave.txt")
#     output:
#         os.path.join(RESULTS_DIR, "assembly/{rtype}/{tool}/ASSEMBLY.FILTERED.fasta")
#     params:
#         minlen=1000,
#         mincov=5
#     conda:
#         os.path.join(ENV_DIR, "rgi.yaml") # NOTE: need an env. containing Biopython and pandas
#     message:
#         "Assembly: filter contigs"
#     script:
#         os.path.join(SRC_DIR, "filter_contigs.py")