# Preprocessing

##################################################
# Short reads

# Preprocess the short reads using fastp
rule fastp_sr:
    input:
        r1=os.path.join(RESULTS_DIR, "input/{mtype}/sr/R1.fq.gz"),
        r2=os.path.join(RESULTS_DIR, "input/{mtype}/sr/R2.fq.gz")
    output:
        r1=os.path.join(RESULTS_DIR, "preproc/{mtype}/sr/R1.fastp.fastq.gz"),
        r2=os.path.join(RESULTS_DIR, "preproc/{mtype}/sr/R2.fastp.fastq.gz"),
        html=os.path.join(RESULTS_DIR, "preproc/{mtype}/sr/fastp.html"),
        json=os.path.join(RESULTS_DIR, "preproc/{mtype}/sr/fastp.json")
    log:
        "logs/preproc.fastp.{mtype}.sr.log"
    wildcard_constraints:
        mtype="metag|metat"
    threads:
        config["fastp"]["threads"]
    conda:
        os.path.join(ENV_DIR, "fastp.yaml")
    message:
        "Preprocessing short reads: FastP"
    shell:
        "(date && fastp -l {config[fastp][min_length]} -i {input.r1} -I {input.r2} -o {output.r1} -O {output.r2} -h {output.html} -j {output.json} -w {threads} && date) &> {log}"

# QC
rule fastqc_fastp_sr:
    input:
        os.path.join(RESULTS_DIR, "preproc/{mtype}/sr/{rid}.fastp.fastq.gz")
    output:
        html=os.path.join(RESULTS_DIR, "qc/{mtype}/sr/{rid}.fastp_fastqc.html"),
        zip=os.path.join(RESULTS_DIR, "qc/{mtype}/sr/{rid}.fastp_fastqc.zip")
    log:
        "logs/preproc.fastqc.{mtype}.sr.{rid}.log"
    wildcard_constraints:
        mtype="metag|metat",
        rid="|".join(["R1", "R2"])
    threads:
        config["fastqc"]["threads"]
    conda:
        os.path.join(ENV_DIR, "fastqc.yaml")
    message:
        "Preprocessing short reads: FastQC"
    shell:
        "(date && fastqc -q -f fastq -t {threads} -o $(dirname {output.html}) {input} && date) &> {log}"

##################################################
# Long reads

# Basecalling
# NOTE: in basecalling.smk

# QC
rule nanostat_guppy_basecalling:
    input:
        os.path.join(RESULTS_DIR, "preproc/metag/lr/lr.fastq.gz")
    output:
        os.path.join(RESULTS_DIR, "qc/metag/lr/NanoStats.txt")
    log:
        "logs/preproc.nanostats.metag.lr.log"
    conda:
        os.path.join(ENV_DIR, "nanostat.yaml")
    message:
        "Preprocessing long reads: NanoStats"
    shell:
        "(date && NanoStat --fastq {input} --outdir $(dirname {output}) -n $(basename {output}) && date) 2> {log}"

##################################################
# rRNA filtering

rule rm_rrna_bbmap_sr_metat:
    input:
        r1=os.path.join(RESULTS_DIR, "preproc/metat/sr/R1.fastp.fastq.gz"),
        r2=os.path.join(RESULTS_DIR, "preproc/metat/sr/R2.fastp.fastq.gz"),
        refs=[os.path.join(DB_DIR, ref) for ref in config["bbmap"]["rrna_refs"]],
    output:
        r1=os.path.join(RESULTS_DIR, "preproc/metat/sr/R1.norrna.fastq.gz"),
        r2=os.path.join(RESULTS_DIR, "preproc/metat/sr/R2.norrna.fastq.gz"),
        stats=os.path.join(RESULTS_DIR, "preproc/metat/sr/bbmap.rrna.stats")
    log:
        "logs/preproc.bbmap.rrna.metat.sr.log"
    threads:
        config["bbmap"]["threads"]
    conda:
        os.path.join(ENV_DIR, "bbmap.yaml")
    message:
        "Preprocessing metaT short reads: bbmap - rm rRNA reads"
    shell:
        "(date && "
        "refs=$(echo \"{input.refs}\" | tr ' ' ',') && "
        "bbduk.sh in={input.r1} in2={input.r2} out={output.r1} out2={output.r2} ref=${{refs}} refstats={output.stats} "
        "qin=33 qout=33 k=31 rcomp=t ziplevel=9 ordered=t threads={threads} && "
        "date) &> {log}"

##################################################
# Remove host contamination

rule rm_host_bbmap_sr_metag:
    input:
        r1=os.path.join(RESULTS_DIR, "preproc/metag/sr/R1.fastp.fastq.gz"),
        r2=os.path.join(RESULTS_DIR, "preproc/metag/sr/R2.fastp.fastq.gz"),
        refs=expand(os.path.join(DB_DIR, "{bname}.fasta"), bname=config["bbmap"]["host_refs"].keys()) if not config["bbmap"]["host_refs"] is None else []
    output:
        r1=os.path.join(RESULTS_DIR, "preproc/metag/sr/R1.nohost.fastq.gz"),
        r2=os.path.join(RESULTS_DIR, "preproc/metag/sr/R2.nohost.fastq.gz"),
        stats=os.path.join(RESULTS_DIR, "preproc/metag/sr/bbmap.host.stats")
    log:
        "logs/preproc.bbmap.host.metag.sr.log"
    threads:
        config["bbmap"]["threads"]
    conda:
        os.path.join(ENV_DIR, "bbmap.yaml")
    message:
        "Preprocessing metaG short reads: bbmap - rm host contamination"
    shell:
        "(date && "
        "refs=$(echo \"{input.refs}\" | tr ' ' ',') && "
        "bbduk.sh in={input.r1} in2={input.r2} out={output.r1} out2={output.r2} ref=\"${{refs}}\" refstats={output.stats} "
        "qin=33 qout=33 k=31 rcomp=t ziplevel=9 ordered=t threads={threads} && "
        "date) &> {log}"

rule rm_host_bbmap_sr_metat:
    input:
        r1=os.path.join(RESULTS_DIR, "preproc/metat/sr/R1.norrna.fastq.gz"),
        r2=os.path.join(RESULTS_DIR, "preproc/metat/sr/R2.norrna.fastq.gz"),
        refs=expand(os.path.join(DB_DIR, "{bname}.fasta"), bname=config["bbmap"]["host_refs"].keys()) if not config["bbmap"]["host_refs"] is None else []
    output:
        r1=os.path.join(RESULTS_DIR, "preproc/metat/sr/R1.nohost.fastq.gz"),
        r2=os.path.join(RESULTS_DIR, "preproc/metat/sr/R2.nohost.fastq.gz"),
        stats=os.path.join(RESULTS_DIR, "preproc/metat/sr/bbmap.host.stats")
    log:
        "logs/preproc.bbmap.host.metat.sr.log"
    threads:
        config["bbmap"]["threads"]
    conda:
        os.path.join(ENV_DIR, "bbmap.yaml")
    message:
        "Preprocessing metaT short reads: bbmap - rm host contamination"
    shell:
        "(date && "
        "refs=$(echo \"{input.refs}\" | tr ' ' ',') && "
        "bbduk.sh in={input.r1} in2={input.r2} out={output.r1} out2={output.r2} ref=\"${{refs}}\" refstats={output.stats} "
        "qin=33 qout=33 k=31 rcomp=t ziplevel=9 ordered=t threads={threads} && "
        "date) &> {log}"

rule rm_host_bbmap_lr_metag:
    input:
        lr=os.path.join(RESULTS_DIR, "preproc/metag/lr/lr.fastq.gz"),
        refs=expand(os.path.join(DB_DIR, "{bname}.fasta"), bname=config["bbmap"]["host_refs"].keys())  if not config["bbmap"]["host_refs"] is None else []
    output:
        lr=os.path.join(RESULTS_DIR, "preproc/metag/lr/lr.nohost.fastq.gz"),
        stats=os.path.join(RESULTS_DIR, "preproc/metag/lr/bbmap.host.stats")
    log:
        "logs/preproc.bbmap.host.metag.lr.log"
    threads:
        config["bbmap"]["threads"]
    conda:
        os.path.join(ENV_DIR, "bbmap.yaml")
    message:
        "Preprocessing metaG long reads: bbmap - rm host contamination"
    shell:
        "(date && "
        "refs=$(echo \"{input.refs}\" | tr ' ' ',') && "
        "bbduk.sh in={input.lr} out={output.lr} ref=\"${{refs}}\" refstats={output.stats} "
        "qin=33 qout=33 k=31 rcomp=t ziplevel=9 ordered=t threads={threads} && " # qin/qout: issue 124
        "date) &> {log}"

##################################################
# Final files to be used for assembly etc.
localrules: link_preproc_fastq_lr, link_preproc_fastq_sr

rule link_preproc_fastq_lr:
    input:
        os.path.join(RESULTS_DIR, "preproc/metag/lr/lr.fastq.gz") if NO_HOST_FILTERING else os.path.join(RESULTS_DIR, "preproc/metag/lr/lr.nohost.fastq.gz")
    output:
        os.path.join(RESULTS_DIR, "preproc/metag/lr/lr.proc.fastq.gz")
    shell:
        "cd $(dirname {output}) && ln -sf $(basename {input}) $(basename {output})"

rule link_preproc_fastq_sr:
    input:
        r1=os.path.join(RESULTS_DIR, "preproc/{mtype}/sr/R1.fastp.fastq.gz") if NO_HOST_FILTERING else os.path.join(RESULTS_DIR, "preproc/{mtype}/sr/R1.nohost.fastq.gz"),
        r2=os.path.join(RESULTS_DIR, "preproc/{mtype}/sr/R2.fastp.fastq.gz") if NO_HOST_FILTERING else os.path.join(RESULTS_DIR, "preproc/{mtype}/sr/R2.nohost.fastq.gz")
    output:
        r1=os.path.join(RESULTS_DIR, "preproc/{mtype}/sr/R1.proc.fastq.gz"),
        r2=os.path.join(RESULTS_DIR, "preproc/{mtype}/sr/R2.proc.fastq.gz")
    wildcard_constraints:
        mtype="|".join(META_TYPES)
    shell:
        "cd $(dirname {output.r1}) && "
        "ln -sf $(basename {input.r1}) $(basename {output.r1}) && "
        "ln -sf $(basename {input.r2}) $(basename {output.r2})"