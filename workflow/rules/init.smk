# Validation
from snakemake.utils import validate
validate(config, srcdir("../../schemas/config.schema.yaml"))

# Paths
SRC_DIR = srcdir("../scripts")
ENV_DIR = srcdir("../envs")
MOD_DIR = srcdir("../../submodules")

# default executable for snakmake
shell.executable("bash")

# working directory
workdir:
    config["work_dir"]

# TODO: config validation

# More paths
RESULTS_DIR = config["results_dir"]
DB_DIR      = config["db_dir"]

# Input: metaT, short reads
INPUT_T_SR    = []
if config["data"]["metat"]["sr"]["r1"] and config["data"]["metat"]["sr"]["r2"]:
    INPUT_T_SR = list(config["data"]["metat"]["sr"].values())

# Whether host contamination has to be removed - only if host references are given
NO_HOST_FILTERING = config["bbmap"]["host_refs"] is None or len(config["bbmap"]["host_refs"].keys()) == 0

# Assemblers and read types
META_TYPES = ["metag"]
if config["data"]["metat"]["sr"]["r1"] and config["data"]["metat"]["sr"]["r2"]:
    META_TYPES = ["metag", "metat"]
READ_TYPES      = list(config["assemblers"].keys()) # list of read types
ASSEMBLERS      = [y for x in config["assemblers"].values() for y in x] # list of all assemblers
READ_ASSEMBLERS = [y for x in [[(k, vv) for vv in v] for k, v in config["assemblers"].items()] for y in x] # list of (read type, assembler)
READ_ASSEMBLER_PAIRS = assembler_pairs(READ_ASSEMBLERS)

# File extensions of index files created by BWA
BWA_IDX_EXT = ["amb", "ann", "bwt", "pac", "sa"]

# Set links to input files
localrules: link_input_files_lr, link_input_files_sr

rule link_input_files_lr:
    output:
        metag_lr=os.path.join(RESULTS_DIR, "preproc/metag/lr/lr.fastq.gz")
    message:
        "Link input files: LR"
    run:
        import os
        from pathlib import Path
        ifname = os.path.abspath(config["data"]["metag"]["ont"]["fastq"])
        ofname = os.path.join(RESULTS_DIR, "preproc/metag/lr/lr.fastq.gz")
        os.symlink(os.path.abspath(ifname), ofname, target_is_directory=False)

rule link_input_files_sr:
    output:
        metag_sr=expand(os.path.join(RESULTS_DIR, "input/metag/sr/{rid}.fq.gz"), rid={"R1", "R2"}),
        metat_sr=expand(os.path.join(RESULTS_DIR, "input/metat/sr/{rid}.fq.gz"), rid={"R1", "R2"}) if "metat" in META_TYPES else []
    message:
        "Link input files: SR"
    run:
        import os
        from pathlib import Path
        # metaG, SR
        os.symlink(os.path.abspath(config["data"]["metag"]["sr"]["r1"]), os.path.join(RESULTS_DIR, "input/metag/sr/R1.fq.gz"), target_is_directory=False)
        os.symlink(os.path.abspath(config["data"]["metag"]["sr"]["r2"]), os.path.join(RESULTS_DIR, "input/metag/sr/R2.fq.gz"), target_is_directory=False)
        # metaT, SR
        if "metat" in META_TYPES:
            os.symlink(os.path.abspath(config["data"]["metat"]["sr"]["r1"]), os.path.join(RESULTS_DIR, "input/metat/sr/R1.fq.gz"), target_is_directory=False)
            os.symlink(os.path.abspath(config["data"]["metat"]["sr"]["r2"]), os.path.join(RESULTS_DIR, "input/metat/sr/R2.fq.gz"), target_is_directory=False)