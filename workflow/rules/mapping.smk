# Mapping reads to assembly

# BWA index
rule mapping_bwa_idx_asm:
    input:
        os.path.join(RESULTS_DIR, "assembly/{rtype}/{tool}/ASSEMBLY.POLISHED.fasta")
    output:
        expand(os.path.join(RESULTS_DIR, "assembly/{{rtype}}/{{tool}}/ASSEMBLY.POLISHED.{ext}"), ext=BWA_IDX_EXT)
    log:
        "logs/mapping.bwa.index.{rtype}.{tool}.log"
    wildcard_constraints:
        rtype="|".join(READ_TYPES),
        tool="|".join(ASSEMBLERS)
    threads: 1
    params:
        idx_prefix=lambda wildcards, output: os.path.splitext(output[0])[0]
    conda:
        os.path.join(ENV_DIR, "racon.yaml")
    message:
        "Mapping: BWA index for assembly mapping"
    shell:
        "(date && bwa index {input} -p {params.idx_prefix} && date) &> {log}"

# Short reads
rule mapping_bwa_mem_asm_sr:
    input:
        r1=os.path.join(RESULTS_DIR, "preproc/{mtype}/sr/R1.proc.fastq.gz"),
        r2=os.path.join(RESULTS_DIR, "preproc/{mtype}/sr/R2.proc.fastq.gz"),
        asm=os.path.join(RESULTS_DIR, "assembly/{rtype}/{tool}/ASSEMBLY.POLISHED.fasta"),
        idx=expand(os.path.join(RESULTS_DIR, "assembly/{{rtype}}/{{tool}}/ASSEMBLY.POLISHED.{ext}"), ext=BWA_IDX_EXT)
    output:
        os.path.join(RESULTS_DIR, "mapping/{mtype}/{rtype}/{tool}/ASSEMBLY.POLISHED.sr.bam")
    log:
        "logs/mapping.bwa.mem.{mtype}.{rtype}.{tool}.sr.log",
    wildcard_constraints:
        mtype="|".join(META_TYPES),
        rtype="|".join(READ_TYPES),
        tool="|".join(ASSEMBLERS)
    threads:
        config["bwa"]["threads"]
    params:
        idx_prefix=lambda wildcards, input: os.path.splitext(input.idx[0])[0],
        bam_prefix=lambda wildcards, output: os.path.splitext(output[0])[0],
        chunk_size=config["samtools"]["sort"]["chunk_size"]
    conda:
        os.path.join(ENV_DIR, "racon.yaml")
    message:
        "Mapping short reads to assembly w/ BWA"
    shell:
        "(date && "
        "bwa mem -t {threads} {params.idx_prefix} {input.r1} {input.r2} | "
        "samtools view -@ {threads} -SbT {input.asm} | "
        "samtools sort -@ {threads} -m {params.chunk_size} -T {params.bam_prefix} > {output} && "
        "date) &> {log}"

# Long reads
rule mapping_bwa_mem_asm_lr:
    input:
        lr=os.path.join(RESULTS_DIR, "preproc/{mtype}/lr/lr.proc.fastq.gz"),
        asm=os.path.join(RESULTS_DIR, "assembly/{rtype}/{tool}/ASSEMBLY.POLISHED.fasta"),
        idx=expand(os.path.join(RESULTS_DIR, "assembly/{{rtype}}/{{tool}}/ASSEMBLY.POLISHED.{ext}"), ext=BWA_IDX_EXT)
    output:
        os.path.join(RESULTS_DIR, "mapping/{mtype}/{rtype}/{tool}/ASSEMBLY.POLISHED.lr.bam")
    log:
        "logs/mapping.bwa.mem.{mtype}.{rtype}.{tool}.lr.log",
    wildcard_constraints:
        mtype="metag",
        rtype="lr|hy",
        tool="|".join(config["assemblers"]["lr"] + config["assemblers"]["hy"])
    threads:
        config["bwa"]["threads"]
    params:
        idx_prefix=lambda wildcards, input: os.path.splitext(input.idx[0])[0],
        bam_prefix=lambda wildcards, output: os.path.splitext(output[0])[0],
        chunk_size=config["samtools"]["sort"]["chunk_size"]
    conda:
        os.path.join(ENV_DIR, "racon.yaml")
    message:
        "Mapping long reads to assembly w/ BWA"
    shell:
        "(date && "
        "bwa mem -x ont2d -t {threads} {params.idx_prefix} {input.lr} | "
        "samtools view -@ {threads} -SbT {input.asm} | "
        "samtools sort -@ {threads} -m {params.chunk_size} -T {params.bam_prefix} > {output} && "
        "date) &> {log}"

# Hybrid: short + long reads
rule mapping_bwa_mem_asm_hy:
    input:
        sr=os.path.join(RESULTS_DIR, "mapping/{mtype}/{rtype}/{tool}/ASSEMBLY.POLISHED.sr.bam"),
        lr=os.path.join(RESULTS_DIR, "mapping/{mtype}/{rtype}/{tool}/ASSEMBLY.POLISHED.lr.bam")
    output:
        os.path.join(RESULTS_DIR, "mapping/{mtype}/{rtype}/{tool}/ASSEMBLY.POLISHED.hy.bam")
    log:
        "logs/mapping.bwa.mem.{mtype}.{rtype}.{tool}.hy.log"
    wildcard_constraints:
        mtype="metag",
        rtype="hy",
        tool="|".join(config["assemblers"]["hy"])
    threads: 1
    conda:
        os.path.join(ENV_DIR, "racon.yaml")
    message:
        "Mapping: merging short-reads and long-reads mapping results"
    shell:
        "(date && samtools merge {output} {input.sr} {input.lr} && date) &> {log}"

##################################################
# Assembly coverage

# Compute the genome coverage histogram
#   https://bedtools.readthedocs.io/en/latest/content/tools/genomecov.html
rule mapping_asm_genomecov:
    input:
        os.path.join(RESULTS_DIR, "mapping/{mtype}/{rtype}/{tool}/ASSEMBLY.POLISHED.{rtype2}.bam")
    output:
        os.path.join(RESULTS_DIR, "mapping/{mtype}/{rtype}/{tool}/ASSEMBLY.POLISHED.{rtype2}.cov.txt")
    log:
        "logs/bedtools.genomecov.{mtype}.{rtype}.{tool}.{rtype2}.log",
    wildcard_constraints:
        mtype="|".join(META_TYPES),
        rtype="|".join(READ_TYPES),
        rtype2="|".join(READ_TYPES),
        tool="|".join(ASSEMBLERS)
    threads: 1
    conda:
        os.path.join(ENV_DIR, "bedtools.yaml")
    message:
        "Mapping: compute assembly coverage"
    shell:
        "(date && bedtools genomecov -ibam {input} > {output} && date) &> {log}"

rule mapping_asm_genomecov_average:
    input:
        os.path.join(RESULTS_DIR, "mapping/{mtype}/{rtype}/{tool}/ASSEMBLY.POLISHED.{rtype2}.cov.txt")
    output:
        os.path.join(RESULTS_DIR, "mapping/{mtype}/{rtype}/{tool}/ASSEMBLY.POLISHED.{rtype2}.cov.ave.txt")
    log:
        "logs/ave.genomecov.{mtype}.{rtype}.{tool}.{rtype2}.log"
    wildcard_constraints:
        mtype="|".join(META_TYPES),
        rtype="|".join(READ_TYPES),
        rtype2="|".join(READ_TYPES),
        tool="|".join(ASSEMBLERS)
    threads: 1
    params:
        script=os.path.join(SRC_DIR, "coverage.awk")
    message:
        "Mapping: compute average assembly coverage"
    shell:
        "(date && cat {input} | awk -f {params.script} | tail -n+2 > {output} && date) &> {log}"

rule mapping_asm_genomecov_perbase:
    input:
        os.path.join(RESULTS_DIR, "mapping/{mtype}/{rtype}/{tool}/ASSEMBLY.POLISHED.sr.bam")
    output:
        os.path.join(RESULTS_DIR, "mapping/{mtype}/{rtype}/{tool}/ASSEMBLY.POLISHED.sr.cov.perbase")
    wildcard_constraints:
        mtype="|".join(META_TYPES),
        rtype="|".join(READ_TYPES),
        tool="|".join(ASSEMBLERS)
    threads: 1
    conda:
        os.path.join(ENV_DIR, "bedtools.yaml")
    message:
        "Mapping: compute per-base assembly coverage"
    shell:
        "bedtools genomecov -d -ibam {input} > {output}"

##################################################
# Mappability index

# flagstat: overall mapped reads, unmapped reads etc. 
# idxstats: per contig information (used this to subset contigs by length)
# unique: number of uniquely mapped reads
rule mapping_asm_flagstat:
    input:
        os.path.join(RESULTS_DIR, "mapping/{mtype}/{rtype}/{tool}/ASSEMBLY.POLISHED.{rtype2}.bam")
    output:
        flagstat=os.path.join(RESULTS_DIR, "mapping/{mtype}/{rtype}/{tool}/ASSEMBLY.POLISHED.{rtype2}.flagstat.txt"),
        idxstats=os.path.join(RESULTS_DIR, "mapping/{mtype}/{rtype}/{tool}/ASSEMBLY.POLISHED.{rtype2}.idxstats.txt"),
        unique=os.path.join(  RESULTS_DIR, "mapping/{mtype}/{rtype}/{tool}/ASSEMBLY.POLISHED.{rtype2}.unique.txt")
    log:
        "logs/samtools.stats.{mtype}.{rtype}.{tool}.{rtype2}.log"
    wildcard_constraints:
        mtype="|".join(META_TYPES),
        rtype="|".join(READ_TYPES),
        rtype2="|".join(READ_TYPES),
        tool="|".join(ASSEMBLERS)
    conda:
        os.path.join(ENV_DIR, "racon.yaml")
    message:
        "Mapping: assembly coverage stats w/ samtools flagstat"
    shell:
        "(date && "
        "samtools flagstat {input} > {output.flagstat} && "
        "samtools idxstats {input} > {output.idxstats} && "
        "samtools view {input} | grep -v \"XT:A:U\" | uniq | wc -l > {output.unique} && "
        "date) &> {log}"
