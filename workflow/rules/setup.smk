# CASC
rule install_casc:
    output:
        os.path.join(MOD_DIR, "casc/bin/casc")
    log:
        "logs/setup.casc.log"
    params:
        path=os.path.join(MOD_DIR, "casc")
    conda:
        os.path.join(ENV_DIR, "casc.yaml")
    message:
        "Setup: install CASC"
    shell:
        "(cd {params.path} && perl Makefile.PL PREFIX=\"$(realpath .)\" && make && make test && make install) &> {log}"

# MinCED
rule install_minced:
    output:
        os.path.join(MOD_DIR, "minced/minced.jar")
    log:
        "logs/setup.minced.log"
    params:
        path=os.path.join(MOD_DIR, "minced")
    conda:
        os.path.join(ENV_DIR, "minced.yaml")
    message:
        "Setup: install MinCED"
    shell:
        "(cd {params.path} && make && make test) &> {log}"

# OPERA-MS
rule install_operams:
    output:
        os.path.join(MOD_DIR, "operams/installation.done")
    log:
        "logs/setup.operams.log"
    params:
        path=os.path.abspath(os.path.join(MOD_DIR, "operams"))
    conda:
        os.path.join(ENV_DIR, "operams.yaml")
    message:
        "Setup: install OPERA-MS"
    shell:
        "(cd {params.path} && "
        "make || make && " # 
        "perl tools_opera_ms/install_perl_module.pl && "
        "perl OPERA-MS.pl check-dependency && "
        "perl OPERA-MS.pl install-db || " # does not return status 0 even if successful
        "echo \"Status: $?\") &> {log} && touch {output}"

# Download RGI data
rule download_rgi_db:
    output:
        archive=temp(os.path.join(DB_DIR, "rgi/card-data.tar.bz2")),
        json=os.path.join(DB_DIR, "rgi/card.json")
    log:
        "logs/setup.rgi.db.log"
    params:
        db_url=config["rgi"]["db_url"]
    message:
        "Setup: download RGI data"
    shell:
        "(date && "
        "wget -O {output.archive} {params.db_url} --no-check-certificate && "
        "tar -C $(dirname {output.archive}) -xvf {output.archive} && "
        "date) &> {log}"

# Setup RGI: load required DB
# NOTE: to make sure that the same DB is used for all targets
rule setup_rgi_db:
    input:
        os.path.join(DB_DIR, "rgi/card.json")
    output:
        "status/rgi_setup.done"
    log:
        "logs/setup.rgi.setup.log"
    conda:
        os.path.join(ENV_DIR, "rgi.yaml")
    message:
        "Setup: load RGI DB"
    shell:
        "(rgi clean --local && rgi load --card_json {input} --local && rgi database --version --local) &> {log} && touch {output}"

# Reference genome FASTA for bbmap to remove host contamination
rule setup_bbmap_host_ref:
    output:
        os.path.join(DB_DIR, "{bname}.fasta")
    log:
        "logs/setup.bbmap.hostref.{bname}.log"
    params:
        url=lambda wildcards, output: config["bbmap"]["host_refs"][wildcards.bname]
    shell:
        "(wget -O {output}.gz {params.url} && gzip -d {output}.gz) &> {log}"

# R-package segclust2d
rule install_segclust2d:
    output:
        done=os.path.join(MOD_DIR, "segclust2d.installed")
    log:
        out="logs/setup.segclust2d.log"
    conda:
        os.path.join(ENV_DIR, "segclust2d.yaml")
    message:
        "Setup: install R-package seqclust2d"
    script:
        os.path.join(SRC_DIR, "install_segclust2d.R")