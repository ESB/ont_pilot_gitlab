# Annotation

include:
    "../rules/taxonomy.smk"

rule TAXONOMY:
    input:
        # Kraken2
        expand(
            os.path.join(RESULTS_DIR, "taxonomy/kraken2/{rtype_tool}.{db}.{otype}.txt"),
            rtype_tool=["%s.%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS],
            db=config["kraken2"]["db"].keys(),
            otype=["labels", "report"]
        ) if "kraken2" in config["steps_taxonomy"] else [],
        expand(
            os.path.join(RESULTS_DIR, "taxonomy/kraken2/{mtype}.sr.{db}.{otype}.txt"),
            mtype=META_TYPES,
            db=config["kraken2"]["db"].keys(),
            otype=["labels", "report"]
        ) if "kraken2" in config["steps_taxonomy"] else [],
        expand(
            os.path.join(RESULTS_DIR, "taxonomy/kraken2/metag.lr.{db}.{otype}.txt"),
            db=config["kraken2"]["db"].keys(),
            otype=["labels", "report"]
        ) if "kraken2" in config["steps_taxonomy"] else [],
        # Kaiju
        expand(
            os.path.join(RESULTS_DIR, "taxonomy/kaiju/{rtype_tool}.{db}.tsv"),
            rtype_tool=["%s.%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS],
            db=config["kaiju"]["db"].keys()
        ) if "kaiju" in config["steps_taxonomy"] else [],
        expand(
            os.path.join(RESULTS_DIR, "taxonomy/kaiju/{rtype_tool}.{db}.summary.{rank}.tsv"),
            rtype_tool=["%s.%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS],
            db=config["kaiju"]["db"].keys(),
            rank=config["kaiju"]["ranks"]
        ) if "kaiju" in config["steps_taxonomy"] else [],
    output:
        touch("status/taxonomy.done")
