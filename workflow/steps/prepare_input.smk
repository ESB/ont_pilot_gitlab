# Prepare input data

include:
    '../rules/prepare_input.smk'

rule PREPARE_INPUT:
    input:
        INPUT_LINKS
    output:
        touch("status/prepare_input.done")
