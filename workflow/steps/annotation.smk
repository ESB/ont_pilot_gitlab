# Annotation

include:
    "../rules/annotation.smk"

rule ANNOTATION:
    input:
        # prodigal
        expand(
            os.path.join(RESULTS_DIR, "annotation/prodigal/{rtype_tool}/proteins.faa"),
            rtype_tool=["%s/%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS]
        ),
        # minced
        expand(
            os.path.join(RESULTS_DIR, "annotation/minced/{rtype_tool}/minced.txt"),
            rtype_tool=["%s/%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS]
        ) if "minced" in config["steps_annotation"] else [],
        # casc
        expand(
            os.path.join(RESULTS_DIR, "annotation/casc/{rtype_tool}/ASSEMBLY.POLISHED.results.txt"),
            rtype_tool=["%s/%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS]
        ) if "casc" in config["steps_annotation"] else [],
        # plasflow
        expand(
            os.path.join(RESULTS_DIR, "annotation/plasflow/{rtype_tool}/plasflow.tsv"),
            rtype_tool=["%s/%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS]
        ) if "plasflow" in config["steps_annotation"] else [],
        # rgi
        expand(
            os.path.join(RESULTS_DIR, "annotation/rgi/{rtype_tool}/rgi.txt"),
            rtype_tool=["%s/%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS]
        ) if "rgi" in config["steps_annotation"] else [],
        # barrnap
        expand(
            os.path.join(RESULTS_DIR, "annotation/barrnap/{rtype_tool}/{kingdom}.{ext}"),
            rtype_tool=["%s/%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS],
            kingdom=config["barrnap"]["kingdom"],
            ext=["fa", "gff"]
        ) if "barrnap" in config["steps_annotation"] else [],
        # kegg
        expand(
            os.path.join(RESULTS_DIR, "annotation/hmm/kegg/{rtype_tool}/hmm.tblout"),
            rtype_tool=["%s/%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS]
        ) if "kegg" in config["steps_annotation"] else [],
    output:
        touch("status/annotation.done")
