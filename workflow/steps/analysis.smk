# Assembly

include:
    "../rules/analysis.smk"

rule ANALYSIS:
    input:
        # quast
        expand(
            os.path.join(RESULTS_DIR, "analysis/quast/{rtype_tool}/report.tsv"),
            rtype_tool=["%s/%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS]
        ) if "quast" in config["steps_analysis"] else [],
        # mash
        expand(
            os.path.join(RESULTS_DIR, "analysis/mash/{dtype}.dist"),
            dtype=["contigs", "reads"]
        ) if "mash" in config["steps_analysis"] else [],
        # mashmap
        expand(
            os.path.join(RESULTS_DIR, "analysis/mashmap/{combi}.one2one.out"),
            combi=["%s_%s__%s_%s" % (p[0][0], p[0][1], p[1][0], p[1][1]) for p in READ_ASSEMBLER_PAIRS] +
            ["%s_%s__%s_%s" % (p[1][0], p[1][1], p[0][0], p[0][1]) for p in READ_ASSEMBLER_PAIRS]
        ) if "mashmap" in config["steps_analysis"] else [],
        # fastani
        [os.path.join(RESULTS_DIR, "analysis/fastani/many2many.out")] if "fastani" in config["steps_analysis"] else [],
        # mummer
        expand(
            os.path.join(RESULTS_DIR, "analysis/mummer/{combi}.dnadiff.report"),
            combi=["%s_%s__%s_%s" % (p[0][0], p[0][1], p[1][0], p[1][1]) for p in READ_ASSEMBLER_PAIRS] +
            ["%s_%s__%s_%s" % (p[1][0], p[1][1], p[0][0], p[0][1]) for p in READ_ASSEMBLER_PAIRS]
        ) if "mummer" in config["steps_analysis"] else [],
        # cdhit
        expand(
            os.path.join(RESULTS_DIR, "analysis/cdhit/{combi}.faa"),
            combi=["%s_%s__%s_%s" % (p[0][0], p[0][1], p[1][0], p[1][1]) for p in READ_ASSEMBLER_PAIRS] +
            ["%s_%s__%s_%s" % (p[1][0], p[1][1], p[0][0], p[0][1]) for p in READ_ASSEMBLER_PAIRS]
        ) if "cdhit" in config["steps_analysis"] else [],
        # diamond
        expand(
            os.path.join(RESULTS_DIR, "analysis/diamond/{rtype_tool}.{ext}"),
            rtype_tool=["%s_%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS],
            ext=["daa", "tsv"]
        ) if "diamond" in config["steps_analysis"] else [],
        expand(
            os.path.join(RESULTS_DIR, "analysis/diamond/{combi}.{ext}"),
            combi=["%s_%s__%s_%s" % (p[0][0], p[0][1], p[1][0], p[1][1]) for p in READ_ASSEMBLER_PAIRS] +
            ["%s_%s__%s_%s" % (p[1][0], p[1][1], p[0][0], p[0][1]) for p in READ_ASSEMBLER_PAIRS],
            ext=["daa", "tsv"]
        ) if "diamond" in config["steps_analysis"] else [],
        # mmseqs2
        [os.path.join(RESULTS_DIR, "analysis/mmseqs2/clusters.tsv")] if "mmseqs2" in config["steps_analysis"] else [],
        [ # for metaP analysis
            os.path.join(RESULTS_DIR, "analysis/mmseqs2/proteins.faa"),
            os.path.join(RESULTS_DIR, "analysis/mmseqs2/clusters.faa")
        ] if "mmseqs2" in config["steps_analysis"] and "metap" in config["data"] else [],
        # coverage
        expand(
            os.path.join(RESULTS_DIR, "mapping/{mtype}/{rtype_tool}.cov.pergene"),
            mtype=META_TYPES,
            rtype_tool=["{r}/{t}/ASSEMBLY.POLISHED.sr".format(r=r, t=t) for r, t in READ_ASSEMBLERS]
        ) if "cov" in config["steps_analysis"] else [],
        expand(
            os.path.join(RESULTS_DIR, "mapping/metag/{rtype_tool}.cov.{ext}"),
            rtype_tool=["{r}/{t}/ASSEMBLY.POLISHED.sr".format(r=r, t=t) for r, t in READ_ASSEMBLERS],
            ext=["seg.tsv", "segsum.tsv"]
        ) if "cov" in config["steps_analysis"] else [],
    output:
        touch("status/analysis.done")
