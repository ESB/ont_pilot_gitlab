# Preprocessing reads before de novo assembly

include:
    '../rules/preprocessing.smk'

rule PREPROCESSING_LR:
    input:
        fastq=os.path.join(RESULTS_DIR, "preproc/metag/lr/lr.proc.fastq.gz"),
        nanostats=os.path.join(RESULTS_DIR, "qc/metag/lr/NanoStats.txt")
    output:
        touch("status/preprocessing_lr.done")

rule PREPROCESSING_SR:
    input:
        fastqc=expand(os.path.join(RESULTS_DIR, "qc/{mtype}/sr/{rid}.fastp_{ext}"), mtype=META_TYPES, rid=["R1", "R2"], ext=["fastqc.html", "fastqc.zip"]),
        fastq=expand(os.path.join(RESULTS_DIR, "preproc/{mtype}/sr/{rid}.proc.fastq.gz"), rid=["R1", "R2"], mtype=META_TYPES)
    output:
        touch("status/preprocessing_sr.done")
