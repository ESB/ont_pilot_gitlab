# Mapping reads to assembly

include:
    "../rules/mapping.smk"

rule COVERAGE:
    input:
        # default: map metaG (LR/SR/hybrid) to contigs
        metag=expand(
            os.path.join(RESULTS_DIR, "mapping/metag/{rtype_tool}.cov.ave.txt"),
            rtype_tool=["{r}/{t}/ASSEMBLY.POLISHED.{r}".format(r=r, t=t) for r, t in READ_ASSEMBLERS]
        ),
        # if metaT: map metaT (SR) to contigs
        metat=expand(
            os.path.join(RESULTS_DIR, "mapping/metat/{rtype_tool}.cov.ave.txt"),
            rtype_tool=["{r}/{t}/ASSEMBLY.POLISHED.sr".format(r=r, t=t) for r, t in READ_ASSEMBLERS]
        ) if "metat" in META_TYPES else [],
        # metaG/metaT SR coverage
        srcov=expand(
            os.path.join(RESULTS_DIR, "mapping/{mtype}/{rtype_tool}.cov.perbase"),
            mtype=META_TYPES,
            rtype_tool=["{r}/{t}/ASSEMBLY.POLISHED.sr".format(r=r, t=t) for r, t in READ_ASSEMBLERS]
        )
    output:
        touch("status/coverage.done")

rule MAPPABILITY:
    input:
        metag=expand(
            os.path.join(RESULTS_DIR, "mapping/metag/{rtype_tool}.{ext}"),
            rtype_tool=["{r}/{t}/ASSEMBLY.POLISHED.{r}".format(r=r, t=t) for r, t in READ_ASSEMBLERS],
            ext=["flagstat.txt", "idxstats.txt", "unique.txt"]
        ),
        metat=expand(
            os.path.join(RESULTS_DIR, "mapping/metat/{rtype_tool}.{ext}"),
            rtype_tool=["{r}/{t}/ASSEMBLY.POLISHED.sr".format(r=r, t=t) for r, t in READ_ASSEMBLERS],
            ext=["flagstat.txt", "idxstats.txt", "unique.txt"]
        ) if "metat" in META_TYPES else []
    output:
        touch("status/mappability.done")
