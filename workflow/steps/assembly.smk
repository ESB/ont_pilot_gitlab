# Assembly

include:
    "../rules/assembly.smk"
# include:
#     "../rules/mapping.smk"

rule ASSEMBLY:
    input:
        expand(
            os.path.join(RESULTS_DIR, "assembly/{rtype_tool}/ASSEMBLY.fasta"),
            rtype_tool=["%s/%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS],
        )
    output:
        touch("status/assembly.done")

rule POLISHING:
    input:
        expand(
            os.path.join(RESULTS_DIR, "assembly/{rtype_tool}/ASSEMBLY.POLISHED.fasta"),
            rtype_tool=["%s/%s" % (rtype, tool) for rtype, tool in READ_ASSEMBLERS],
        )
    output:
        touch("status/polishing.done")
