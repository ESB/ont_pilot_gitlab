# About

Notes for those trying to reproduce the results from the publication.

# Setup

## Data

Processed sequencing data of the GDB sample is available under BioProject accession PRJNA723028 (Biosamples: metag_sr: SAMN18797629, metat_sr: SAMN18797630, and metag_lr: SAMN18797631).
Metaproteomics data of the GDB sample is available at ProteomeXchange under accession PXD025505.
The code used for the analysis is available at https://doi.org/10.6084/m9.figshare.14447553 and supplementary data of relevant results is available at https://doi.org/10.6084/m9.figshare.14447559.

Note: For metaP data access, see also `notes/gdb_metap.md`.

## Conda and snakemake

Install `conda` and create the main `snakemake` environment (see `README.md`).

## Other tools

Clone `OPERA-MS` repository
```bash
# in the code directory
git clone https://github.com/CSB5/OPERA-MS/tree/c18b4f3c933603a7b35d0ea601a80417fe783964 submodules/operams
```

## Databases

All database files need to be in the same folder (can also be symlinks) defined in all sample config files in `db_dir`.
Some databases will be downloaded/created by the pipeline and some are not required (see below).

The following database file names/paths can be defined as empty strings or lists in the sample config files:
- `bbmap:rrna_refs` as empty list (`bbmap:host_refs` can be kept unchanged)
- `hmm:kegg` as empty string
- `kraken2:db` as empty string
- `kaiju:db` as empty string
- `GTDBTK:DATA` as empty string

### UniProtKB/TrEMBL database (DIAMOND format)

You might need to change the URL shown below to download the version from Aug 24 2019.

```bash
# may require a large amount of RAM
wget -q ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_trembl.fasta.gz 
diamond makedb --in uniprot_trembl.fasta.gz -d uniprot_trembl
```

The name of the created `*.dmnd` file has to be set in all sample config files in `diamond:db`.

## Workflows

The bash scripts to run the `snakemake` pipelines assume that `slurm` is used to submit the jobs.
They have to be modified if this is not the case.

For other notes, see `README.md`.

## GDB

Note, that for GDB, the publicly available metaG/metaT sequencing data has been already processed.
That means that the preprocessing step in the main workflow has to be skipped:

- remove "preprocessing" from the list of the attribute `steps` in `config/gdb/config.yaml`
- the following files need to exist (can also be symlinks)
  - metaG, SR: `R1.proc.fastq.gz` and `R2.proc.fastq.gz` in `YOURPATH/results/preproc/metag/sr/`
  - metaT, SR: `R1.proc.fastq.gz` and `R2.proc.fastq.gz` in `YOURPATH/results/preproc/metat/sr/`
  - metaG, LR: `YOURPATH/results/preproc/metag/lr/lr.proc.fastq.gz`

where `YOURPATH` is the set value for `work_dir` in `config/gdb/config.yaml`.
