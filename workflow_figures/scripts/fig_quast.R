#!/usr/bin/Rscript

# Figure: QUAST statistics

############################## LOG
sink(file=file(snakemake@log[[1]], open="wt"), type=c("output", "message"))

############################## LIBS
suppressMessages(library(testit)) # assertions
suppressMessages(library(dplyr)) # tables
suppressMessages(library(patchwork)) # combine plots

# custom
source(snakemake@params$const)
source(snakemake@params$utils)

############################## CONFIG

# assemblers
ASM_NAMES_CONFIG <- c(
    snakemake@config$assemblers$lr,
    snakemake@config$assemblers$sr,
    snakemake@config$assemblers$hy
)
ASM_TOOL_NAMES  <- ASM_TOOL_NAMES[ASM_NAMES_CONFIG]
ASM_TOOL_COLORS <- ASM_TOOL_COLORS[ASM_NAMES_CONFIG]; names(ASM_TOOL_COLORS) <- ASM_TOOL_NAMES[names(ASM_TOOL_COLORS)]
ASM_TOOL_SHAPES <- ASM_TOOL_SHAPES[ASM_NAMES_CONFIG]; names(ASM_TOOL_SHAPES) <- ASM_TOOL_NAMES[names(ASM_TOOL_SHAPES)]

############################## DATA

# individual tables
TABS <- list()
for(sname in names(snakemake@config$samples)){
    testit::assert(sname %in% names(snakemake@input))
    TABS[[sname]] <- read_quast(snakemake@input[[sname]])
}

############################## PLOT

# individual plots
FIGS <- list()
for(sname in names(snakemake@config$samples)){
    FIGS[[sname]] <-
        plot_quast(TABS[[sname]]) +
        labs(title=SAMPLE_NAMES[sname]) +
        theme(
            plot.title=element_text(size=16, face="bold"),
            plot.subtitle=element_text(size=16, face="italic"),
            # legend
            legend.title=element_text(size=16, face="bold"),
            legend.text=element_text(size=16),
            # strip
            strip.text=element_text(size=14),
            # axes
            axis.title=element_text(size=16, color="black"),
            axis.text.y=element_text(size=16, color="black"),
            axis.text.x=element_text(size=16, color="black")
        )
    # remove axis labels for first sub-plots (4 plots in a 2x2 layout)
    if(which(names(SAMPLE_NAMES) == sname) < 3){
        FIGS[[sname]] <- FIGS[[sname]] + theme(axis.text.x=element_blank())
    }
}

# combine plots
FIGS_ALL <- Reduce("+", FIGS) + plot_layout(ncol=2)

# PDF
pdf(snakemake@output$pdf, width=20, height=14)
print(FIGS_ALL)
dev.off()