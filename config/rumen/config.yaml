############################################################
# STEPS

# Steps to be done
# steps: ["preprocessing", "assembly", "mapping", "annotation", "analysis", "taxonomy"]
# steps_annotation: ["rgi", "plasflow", "minced", "barrnap"] # prodigal is run in any case; excluded: kegg
# steps_analysis: ["quast", "mash", "mashmap", "fastani", "cdhit", "diamond", "mmseqs2", "cov"] # excluded: mummer
# steps_taxonomy: ["kraken2", "kaiju"]
steps: ["preprocessing", "assembly", "mapping", "annotation", "analysis"]
steps_annotation: ["rgi", "barrnap"] # prodigal is run in any case
steps_analysis: ["quast", "mash", "diamond", "mmseqs2", "cov"]
steps_taxonomy: []

############################################################
# INPUT

# working directory: will contain the results (should be writeable)
work_dir: "/scratch/users/vgalata/rumen"

# Paths WITHIN the working directory
# directory containing required DBs (should be writeable)
db_dir: "/mnt/lscratch/users/vgalata/ONT_pilot_DBs"
# results directory (will be created in work_dir)
results_dir: "results"

# Data paths: Use absolute paths or paths relative to the working directory !!!
data:
    # Meta-genomics
    metag:
        sr: 
            r1: "/mnt/isilon/projects/ecosystem_biology/ONT_pilot/external_data/rumen/sr/10572_0012_R1.fastq.gz"
            r2: "/mnt/isilon/projects/ecosystem_biology/ONT_pilot/external_data/rumen/sr/10572_0012_R2.fastq.gz"
        ont:
            fastq: "/scratch/users/vgalata/rumen/basecalling/lr.fastq.gz"
    # Meta-transcriptomics
    metat:
        sr:
            r1: "" # leave empty if no data, i.e. ""
            r2: "" # leave empty if no data, i.e. ""
    # Meta-proteomics
    # metap:
        # TODO

############################################################
# TOOLS

##############################
# Preprocessing

# https://github.com/OpenGene/fastp
fastp:
    threads: 10
    min_length: 40

# https://www.bioinformatics.babraham.ac.uk/projects/fastqc/
fastqc:
    threads: 10

##############################
# Assembly

# List of assemblers for different read types: assembler names MUST be UNIQUE
assemblers:
    sr: ["megahit", "metaspades"]
    lr: ["flye", "raven"]
    hy: ["metaspadeshybrid", "operamsmegahit", "operamsmetaspades"]

# https://github.com/fenderglass/Flye
flye:
    threads: 10

# https://canu.readthedocs.io/en/latest/
canu:
    threads: 24
    genome_size: "1g"

# https://github.com/lbcb-sci/raven
raven:
    threads: 10

# https://github.com/ablab/spades
metaspades:
    threads: 10

# https://github.com/voutcn/megahit
megahit:
    threads: 10

# https://github.com/CSB5/OPERA-MS
operams:
    threads: 12

##############################
# Assembly polishing

# https://nanoporetech.github.io/medaka/index.html
medaka:
    threads: 10 # NOTE: avoid large values !!! e.g. 30 did not work
    model: r941_min_high # the MinION model, high accuarcy

# https://github.com/isovic/racon
racon:
    threads: 30

##############################
# Mapping

# http://bio-bwa.sourceforge.net/
bwa:
    threads: 10

# http://www.htslib.org/doc/samtools.html
samtools:
    sort:
        chunk_size: "4G"
        chunk_size_bigmem: "16G"

##############################
# Annotation

# https://github.com/bbuchfink/diamond
diamond:
    threads: 20
    db: "nr_uniprot_trembl.dmnd" # file name in "dbs" folder

# https://github.com/dnasko/CASC
casc:
    threads: 10

# https://github.com/ctSkennerton/minced
# minced:

# https://github.com/smaegol/PlasFlow
# plasflow:
#     threshold: 0.7 # class. prob. threshold
#     minlen: 1000 # rm contigs with length below this threshold

# https://github.com/arpcard/rgi
rgi:
    threads: 5
    db_url: "https://card.mcmaster.ca/latest/data"

# https://github.com/tseemann/barrnap
barrnap:
    threads: 5
    kingdom: ["bac", "arc", "euk", "mito"]

##############################
# Analysis

# https://github.com/weizhongli/cdhit --> wiki
cdhit:
    threads: 10

# https://sourceforge.net/projects/bbmap/
# https://github.com/BioInfoTools/BBMap/
bbmap:
    threads: 10
    rrna_refs: [ # file names in "dbs" folder
        "sortmerna/rfam-5.8s-database-id98.fasta",
        "sortmerna/rfam-5s-database-id98.fasta",
        "sortmerna/silva-arc-16s-id95.fasta",
        "sortmerna/silva-arc-23s-id98.fasta",
        "sortmerna/silva-bac-16s-id90.fasta",
        "sortmerna/silva-bac-23s-id98.fasta",
        "sortmerna/silva-euk-18s-id95.fasta",
        "sortmerna/silva-euk-28s-id98.fasta"
    ]
    host_refs: null

# HMMs
hmm:
    threads: 10
    kegg: "KO_cdhitGe10000_160314.hmm"

# Assembly quality
# https://github.com/ablab/quast
quast:
    threads: 10

# https://github.com/marbl/mash
mash:
    threads: 10

# https://github.com/marbl/MashMap
mashmap:
    threads: 10

# https://github.com/ParBLiSS/FastANI
fastani:
    threads: 10

# https://github.com/soedinglab/MMseqs2
mmseqs2:
    threads: 5

##############################
# Taxonomy

# https://ccb.jhu.edu/software/kraken2/
# https://github.com/DerrickWood/kraken2
kraken2:
    threads: 10
    db: # dir. name in "dbs" folder
        maxikraken: "maxikraken2_1903_140GB"

# http://kaiju.binf.ku.dk/
# http://kaiju.binf.ku.dk/server
# https://github.com/bioinformatics-centre/kaiju
kaiju:
    threads: 10
    db: # dir. name in "dbs" folder
        # key = basename of *.fmi
        kaiju_db_nr_euk: "kaiju_db_nr_euk_2020-05-25"
    ranks: ["phylum", "class", "order", "family", "genus", "species"]

# https://github.com/Ecogenomics/GTDBTk
GTDBTK: # dir. name in "dbs" folder
    DATA: "gtdbtk_release89"
