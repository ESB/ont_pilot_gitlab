#!/bin/bash -l

# slurm settings if called using sbatch
#SBATCH -J ONT_SMK
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --time=2-00:00:00
#SBATCH -p batch
#SBATCH -q normal

# conda env name or path
SMK_ENV=$1
# where to create snakemake's conda env.s (path)
SMK_CONDA=$2
# config files
SMK_CONFIG="config/rawdata/config.yaml"
SMK_SLURM="config/rawdata/slurm.yaml"
# slurm cluster call
SMK_CLUSTER="sbatch --partition {cluster.partition} {cluster.qos} {cluster.explicit} --nodes {cluster.nodes} \
--ntasks {cluster.ntasks} --cpus-per-task {threads} --time {cluster.time} --job-name={cluster.job-name}"

conda activate ${SMK_ENV} && \
snakemake -s workflow_rawdata/Snakefile -rp --jobs 5 --local-cores 1 \
--configfile ${SMK_CONFIG} --use-conda --conda-prefix ${SMK_CONDA} \
--cluster-config ${SMK_SLURM} --cluster "${SMK_CLUSTER}"
