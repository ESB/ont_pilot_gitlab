#!/bin/bash -l

# execute from base code directory

ARCHIVE="/scratch/users/vgalata/ONT_pilot_manuscript/code_archive/$(date +"%Y%m%d")"

mkdir -p "${ARCHIVE}"

# copy data
rsync -rtP --prune-empty-dirs --links --exclude=".git*" --exclude=".snakemake" --exclude="submodules" --exclude="*.pyc" . "${ARCHIVE}/" && \
cd "$(dirname ${ARCHIVE})" && \
tar -cvf - "$(basename ${ARCHIVE})" | pigz --best -p 1 > "$(basename ${ARCHIVE}).tar.gz"
