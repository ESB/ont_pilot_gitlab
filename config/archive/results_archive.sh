#!/bin/bash -l

# execute from base code directory

ARCHIVE="/scratch/users/vgalata/ONT_pilot_manuscript/results_archive/$(date +"%Y%m%d")"

mkdir -p "${ARCHIVE}"

# copy data
SAMPLES="zymo nwc gdb rumen"
# SAMPLES="zymo"
for SNAME in ${SAMPLES}; do
    echo "Copy data for ${SNAME} into ${ARCHIVE}"
    rsync -rtP --copy-links --prune-empty-dirs \
    --include="*/" --include-from="config/archive/results_archive_incl.txt" \
    --exclude="*" "/scratch/users/vgalata/${SNAME}/results/" "${ARCHIVE}/${SNAME}/"
done && \
rsync -rtP "README_ARCHIVE.md" "${ARCHIVE}/" && \
cd "$(dirname ${ARCHIVE})" && \
tar -cvf - "$(basename ${ARCHIVE})" | pigz --best -p 5 > "$(basename ${ARCHIVE}).tar.gz"
