#!/bin/bash -l

# conda env name or path
SMK_ENV=$1
# where to install snakemake's conda environments
SMK_CONDA=$2

# config files
SMK_CONFIG="config/gdb/config.yaml config/nwc/config.yaml config/rumen/config.yaml config/zymo/config.yaml"

# activate main env. containing snakemake
conda activate "${SMK_ENV}"

# create reports
for configfile in ${SMK_CONFIG}; do
    echo "Report using ${configfile}"
    snakemake -s "workflow_report/Snakefile" --cores 1 --configfile "${configfile}" --use-conda --conda-prefix "${SMK_CONDA}" -rp
done
