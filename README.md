# About

Comparing genome and gene reconstruction when using short reads (SR) (Illumina) only, long reads (LR) (Oxford Nanopore Technology) only and a hybrid approach (Hy).

See `README_REPROD.md` for additional information on how to run the analysis on a different system than the Uni Luxembourg's HPC server.

# Setup

## Conda

[Conda user guide](https://docs.conda.io/projects/conda/en/latest/user-guide/index.html)

```bash
# install miniconda3
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod u+x Miniconda3-latest-Linux-x86_64.sh
./Miniconda3-latest-Linux-x86_64.sh # follow the instructions
```

Create the main `snakemake` environment

```bash
# create venv
conda env create -f requirements.yml -n "YourEnvName"
```

## Configs

All config files are stored in the folder `config/`:

- raw data workflow: `rawdata/`
  - does not include GDB
- FAST5 and main workflow: samples: Zymo (`zymo/`), NWC (`nwc/`), GDB (`gdb/`) and Rumen (`rumen/`) including
  - config YAML file (`config*.yaml`) for a `Snakemake` workflow
  - `slurm` config YAML file (`slurm*.yaml`) for a `Snakemake` workflow
  - bash script (`sbatch*.sh`) to execute a `Snakemake` workflow
- report workflow:
  - bash script to create sample reports: `reports.sh`
- figures workflow:
  - config YAML file to create figures: `fig.yaml`

## Databases

All database files need to be in the same folder (can also be symlinks) defined in all sample config files in `db_dir`.

# Workflows

1. Raw data workflow: download public datasets (*Note: GDB is not included*)
2. FAST5 workflow (per sample): process FAST5 files
3. Main workflow (per sample): data analysis
4. Report workflow (per sample): create sample reports
5. Figures workflow: create figures for the paper

Relevant paremters which have to be changed are listed for each workflow and config file.
Parameters defining system-relevant settings are not listed but should be also be changed if required, e.g. number of threads used by certain tools etc.

## Raw data workflow

Download raw data required for the analysis.

- config: `config/rawdata/`
  - `config.yaml`:
    - change `work_dir`
  - `sbatch.sh`
    - change `SMK_ENV`
    - if not using `slurm` to submit jobs remove `--cluster-config`, `--cluster` from the `snakemake` CMD
  - `slurm.yaml` (only relevant if using `slurm` for job submission)
- workflow: `workflow_rawdata/`

## FAST5 workflow

Process raw FAST5 files of a sample
- create a multi-FAST5 file from single-FAST5 files
- do basecalling

This step is **not** required if the long-read FASTQ file is already available.
If **not** running this step make sure to provide the correct path in `config/<sample>/config.yaml` for the attribute `data:metag:ont:fastq`.

- config:
  - per sample
  - `config/<sample>/config.fast5.yaml`
    - change `work_dir`, `single_fast5_dir`, `multi_fast5_dir`, `basecalling_dir`
    - change `guppy:gpu:path` and `gyppy:gpu:bin`
  - `config/<sample>/sbatch.fast5.yaml`
    - change `SMK_ENV`
    - if not using `slurm` to submit jobs remove `--cluster-config`, `--cluster` from the `snakemake` CMD
  - `config/<sample>/slurm.fast5.yaml` (only relevant if using `slurm` for job submission)
- workflow: `workflow_fast5/`

## Main workflow

Main analysis workflow: given SR and LR FASTQ files, run all the steps to generate required output. This includes:

- read preprocessing and QC
- assembly and assembly polishing
- mapping reads to assembly (mapping rate and coverage)
- gene calling and annotation
- additional analyses of the assemblies and their annotations
- taxonomic analysis (optional)

The workflow is run per sample and might require a couple of days to run depending on the sample, used configuration and available computational resources.
Note that the workflow will create additional output files not necessarily required to re-create the figures shown in the manuscript.

- config:
  - per sample
  - `config/<sample>/config.yaml`
    - change all path parameters (not all databases are required, see above)
  - `config/<sample>/sbatch.yaml`
    - change `SMK_ENV`
    - if not using `slurm` to submit jobs remove `--cluster-config`, `--cluster` from the `snakemake` CMD
  - `config/<sample>/slurm.yaml` (only relevant if using `slurm` for job submission)
- workflow: `workflow/`

## Report workflow

This workflow creates various summary files, plots and an HTML report for a sample using the output of the main workflow.

Note: How the metaP peptide/protein reports were generated from raw metaP data is described in `notes/gdb_metap.md`.

- config:
  - sample configs used for the main workflow
- workflow: `workflow_report/`

To execute this workflow for all samples:
```bash
./config/reports.sh "YourEnvName" "WhereToCreateCondEnvs"
```

## Figures workflow

Re-create figures (and tables) used in the manuscript.
This workflow should be only run after running the main workflow and report workflow for all samples.

- config: `config/fig.yaml`
  - change `work_dir`
  - change paths for all samples in `samples`
- workflow: `workflow_figures/`

```bash
conda activate "YourEnvName"
snakemake -s workflow_figures/Snakefile --cores 1 --configfile config/fig.yaml --use-conda --conda-prefix "WhereToCreateCondEnvs" -rpn # dry-run
```

# Notes

Notes for manual/additional analyses done using the generated data.

- `gdb_metap.md`: how the metaP reports were generated
- `gdb_mmseqs2_highconf.md`: sample GDB, "high-confidence" proteins and protein clusters (assembly intersection and metaT cov.)
- `gdb_rgi_aro3004454_flye.md`: sample GDB, an RGI example for diff. between SR/Hy and LR, and metaT coverage
- `gdb_mmseqs2_uniq_metat.md`: sample GDB, unique genes and their metaT coverage
- `gdb_rgi_other.md`: sample GDB, other RGI examples for diff. between SR/Hy and LR, and metaT coverage
- `gdb_barrnap_metat.md`: sample GDB, metaT cov. of some predicted rRNA genes (barrnap)