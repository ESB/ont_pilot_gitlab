# About

Additional analysis focusing on AMR, i.e. RGI (CARD) results, in GDB.

Based in file `amr.tsv`:
- table created using `workflow_report` (2021.03.10)
- all RGI hits to AROs which were found (non-nudged hits) in SR/Hy but **NOT** in LR
- `amr.xlsx`: annotated table

# Overview

- LR
  - only nudged hits
  - some hits have >=10x metaT cov.
    - LR, Flye: `ARO:3002999`, `ARO:3004454` (see `notes/gdb_rgi_aro3004454_flye.md`)
    - LR, Raven: `ARO:3002999`
- SR/HY
  - only few nudged hits
  - all AROs have at least one non-nudged hit in each assembly
  - most hits are >= 10x cov. metaT cov, only two have ca. 7x
  - `ARO:3004454` has no perfect hits in any assembly
  - other two AROs at least one perfect hit in each assembly

# ARO:3000194 and MMSEQS2 cluster

```bash
# getting cluster representative for the perfect hit in MEGAHIT
grep -P "\tmegahit__k141_88921_2$" /scratch/users/vgalata/gdb/results/analysis/mmseqs2/clusters.tsv
# operamsmetaspades__opera_contig_2773_1  megahit__k141_88921_2

# getting all cluster members
grep -P "^operamsmetaspades__opera_contig_2773_1\t" /scratch/users/vgalata/gdb/results/analysis/mmseqs2/clusters.tsv | cut -f2
# operamsmetaspades__opera_contig_2773_1
# flye__contig_70:1.0-89743.0_55
# metaspadeshybrid__NODE_230_length_104252_cov_9.790458_35
# metaspadeshybrid__NODE_348_length_69013_cov_11.179239_18
# metaspades__NODE_8958_length_2718_cov_51.879591_1
# operamsmegahit__opera_contig_1206_6
# megahit__k141_88921_2
```

- HY OPERA-MS (metaSPAdes): `opera_contig_2773_1`: the only perfect hit
- LR Flye: `contig_70:1.0-89743.0_55`: RGI hit to `ARO:3004442` (tet(W/N/W)) (strict hit, not nudged)
- HY metaSPAdes: `NODE_230_length_104252_cov_9.790458_35`: one of two perfect hits
- HY metaSPAdes: `NODE_348_length_69013_cov_11.179239_18`: one of two perfect hits
- SR metaSPAdes: `NODE_8958_length_2718_cov_51.879591_1`: the only pefect hit
- HY OPERA-MS (MEGAHIT): `opera_contig_1206_6`: the only pefect hit
- SR MEGAHIT: `k141_88921_2`: the only perfect hit

`ARO:3000194` and `ARO:3004442` are very similar: identity 608/639 (95.1%), similarity 623/639 (97.5%), no gaps.
From (EMBOSS Needle)[https://www.ebi.ac.uk/Tools/psa/emboss_needle/]:
```
+                  1 MKIINIGILAHVDAGKTTLTESLLYASGAISEPGSVEKGTTRTDTMFLER     50
                     ||||||||||||||||||||||||||||||||||||||||||||||||||
+                  1 MKIINIGILAHVDAGKTTLTESLLYASGAISEPGSVEKGTTRTDTMFLER     50

+                 51 QRGITIQAAVTSFQWHRCKVNIVDTPGHMDFLAEVYRSLAVLDGAILVIS    100
                     ||||||||||||||||||||||||||||||||||||||||||||||||||
+                 51 QRGITIQAAVTSFQWHRCKVNIVDTPGHMDFLAEVYRSLAVLDGAILVIS    100

+                101 AKDGVQAQTRILFHALRKMNIPTVIFINKIDQAGVDLQSVYQSVRDKLSA    150
                     ||||||||||||||||||||||||||||||||||||||||.|||||||||
+                101 AKDGVQAQTRILFHALRKMNIPTVIFINKIDQAGVDLQSVVQSVRDKLSA    150

+                151 DIIIKQTVSLSPEIVLEENTDIEAWDAVIENNDELLEKYIAGEPISREKL    200
                     ||||||||||||||||||||||||||||||||||||||||||||||||||
+                151 DIIIKQTVSLSPEIVLEENTDIEAWDAVIENNDELLEKYIAGEPISREKL    200

+                201 AREEQQRVQDASLFPVYHGSAKNGLGIQPLMDAVTGLFQPIGEQGGAALC    250
                     ||||||||||||||||||||||||||||||||||||||||||||||||||
+                201 AREEQQRVQDASLFPVYHGSAKNGLGIQPLMDAVTGLFQPIGEQGGAALC    250

+                251 GSVFKVEYTDCGQRLVYLRLYSGTLRLRDTVALAGREKLKITEMRIPSKG    300
                     ||||||||||||||.|||||||||||||||||||||||||||||||||||
+                251 GSVFKVEYTDCGQRRVYLRLYSGTLRLRDTVALAGREKLKITEMRIPSKG    300

+                301 EIVRTDTAHKGEIVILPSDSLRLNDILGDKTQLPREMWSDVPFPMLRTTI    350
                     ||||||||::||||||||||:||||:|||:|:|||:.|.:.|.|||||||
+                301 EIVRTDTAYQGEIVILPSDSVRLNDVLGDQTRLPRKRWREDPLPMLRTTI    350

+                351 TPKTAEQRDRLLDALTQIADTDPLLHYEVDSITHEIILSFLGRVQLEVVS    400
                     .||||.||:||||||||:|||||||..|||||||||||||||||||||||
+                351 APKTAAQRERLLDALTQLADTDPLLRCEVDSITHEIILSFLGRVQLEVVS    400

+                401 ALLSEKYKLETVVKEPTVIYMERPLKAASHTIHIEVPPNPFWASIGLSVT    450
                     ||||||||||||||||:|||||||||||||||||||||||||||||||||
+                401 ALLSEKYKLETVVKEPSVIYMERPLKAASHTIHIEVPPNPFWASIGLSVT    450

+                451 PLPLGSGVQYESRVSLGYLNQSFQNAVRDGIRYGLEQGLFGWNVTDCKIC    500
                     ||.|||||||||||||||||||||||||||||||||||||||||||||||
+                451 PLSLGSGVQYESRVSLGYLNQSFQNAVRDGIRYGLEQGLFGWNVTDCKIC    500

+                501 FEYGLYYSPVSTPADFRSLAPIVLEQALKESGTQLLEPYLSFTLYAPREY    550
                     ||||||||||||||||||||||||||||||||||||||||||.||||:||
+                501 FEYGLYYSPVSTPADFRSLAPIVLEQALKESGTQLLEPYLSFILYAPQEY    550

+                551 LSRAYHDAPKYCATIETVQVKKDEVVFTGEIPARCIQAYRTDLAFYTNGR    600
                     |||||||||||||||||.||||||||||||||||||||||||||||||||
+                551 LSRAYHDAPKYCATIETAQVKKDEVVFTGEIPARCIQAYRTDLAFYTNGR    600

+                601 SVCLTELKGYQATVGEPIIQPRRPNSRLDKVRHMFSKIP    639
                     ||||||||||||.||:|:|||||||||||||||||.|:.
+                601 SVCLTELKGYQAAVGQPVIQPRRPNSRLDKVRHMFQKVM    639
```

Conclusions:
- the cluster represents all perfect hits to this ARO
- only one outlier - the hit in `Flye` assembly which mapped to a different but very similar ARO

# metaP

``` bash
cat ~/tmp/gdb_amr_subset.txt | while read pid; do echo -e "${pid}: $(grep -F \'${pid}\' all_proteins_1_1_Default_Peptide_Report.txt | wc -l)"; done
# TODO: to be updated
```