# About

Notes for samples `GDB` and genes/proteins identified as "unique" using `mmseqs2` clustering results.

# Number of (metaT covered) unique genes/proteins

File `report/mmseqs2_uniq.tsv` is created by `workflow_report`.

```python
# Per assembly, compute the total number of unique genes,
# and number and percentage of unique genes with ave. metaT coverage
# of at least 10x.

import pandas
df = pandas.read_csv("/scratch/users/vgalata/gdb/results/report/mmseqs2_uniq.tsv", sep="\t", header=0)
for gr_value, gr_df in df.groupby(axis=0, by=["tool"]): # group by assembly/tool
    total = gr_df.shape[0]
    count = sum(gr_df["ave_cov"] >= 10) # ave. metaT coverage >= 10x
    pct   = 100 * count / total
    print("%s: %d, %d, %.2f" % (gr_value, total, count, pct))

# flye: 51342, 6273, 12.22
# megahit: 16672, 1651, 9.90
# metaspades: 13860, 1131, 8.16
# metaspadeshybrid: 26679, 1891, 7.09
# operamsmegahit: 11755, 1092, 9.29
# operamsmetaspades: 10807, 824, 7.62
# raven: 34158, 5245, 15.36
```