# About

Notes for sample `GDB` and its RGI (CARD) results.
Based on results produced by `workflow` and the file `report/amr.tsv` created by `workflow_report`.

# Hits in contig_5026:14.0-10236.0 (LR, Flye) to ARO:3004454

Two hits in  contig `contig_5026:14.0-10236.0` from `GDB`'s LR-assembly constructed with `Flye` to `ARO:3004454`:
- `contig_5026:14.0-10236.0_8`
- `contig_5026:14.0-10236.0_9`

## ARO:3004454

[ARO:3004454](https://card.mcmaster.ca/ontology/41665)

```
>gb|AAA23018.1|+|Campylobacter coli chloramphenicol acetyltransferase [Campylobacter coli]
MQFTKIDINNWTRKEYFDHYFGNTPCTYSMTVKLDISKLKKDGKKLYPTLLYGVTTIINRHEEFRTALDENGQVGVFSEMLPCYTVFHKE
TETFSSIWTEFTADYTEFLQNYQKDIDAFGERMGMSAKPNPPENTFPVSMIPWTSFEGFNLNLKKGYDYLLPIFTFGKYYEEGGKYYIPL
SIQVHHAVCDGFHVCRFLDELQDLLNK

>gb|M35190.1|+|309-932|Campylobacter coli chloramphenicol acetyltransferase [Campylobacter coli]
ATGCAATTCACAAAGATTGATATAAATAATTGGACACGAAAAGAGTATTTCGACCACTATTTTGGCAATACGCCCTGCACATATAGTATG
ACGGTAAAACTCGATATTTCTAAGTTGAAAAAGGATGGAAAAAAGTTATACCCAACTCTTTTATATGGAGTTACAACGATCATCAATCGA
CATGAAGAGTTCAGGACCGCATTAGATGAAAACGGACAGGTAGGCGTTTTTTCAGAAATGCTGCCTTGCTACACAGTTTTTCATAAGGAA
ACTGAAACCTTTTCGAGTATTTGGACTGAGTTTACAGCAGACTATACTGAGTTTCTTCAGAACTATCAAAAGGATATAGACGCTTTTGGT
GAACGAATGGGAATGTCCGCAAAGCCTAATCCTCCGGAAAACACTTTCCCTGTTTCTATGATACCGTGGACAAGCTTTGAAGGCTTTAAC
TTAAATCTAAAAAAAGGATATGACTATCTACTGCCGATATTTACGTTTGGGAAGTATTATGAGGAGGGCGGAAAATACTATATTCCCTTA
TCGATTCAAGTGCATCATGCCGTTTGTGACGGCTTTCATGTTTGCCGTTTTTTGGATGAATTACAAGACTTGCTGAATAAATAA
```

## Matched proteins

Protein information from `Prodigal`'s FAA file:
```bash
grep "^>contig_5026:14.0-10236.0_" /scratch/users/vgalata/gdb/results/annotation/prodigal/lr/flye/proteins.faa
# >contig_5026:14.0-10236.0_8 # 5547 # 5816 # 1 # ID=4198_8;partial=00;start_type=ATG;rbs_motif=GGAGG;rbs_spacer=3-4bp;gc_cont=0.356
# >contig_5026:14.0-10236.0_9 # 5788 # 6174 # 1 # ID=4198_9;partial=00;start_type=ATG;rbs_motif=3Base/5BMM;rbs_spacer=13-15bp;gc_cont=0.377
```

- consecutive CDSs
- both CDS overlap

Nucleotide and protein sequences of both CDS/proteins:
```
>contig_5026:14.0-10236.0_8_nucl
ATGATGCAATTCACAAAGATTGATATAAATAATTGGACACGAAAAGAGTATTTCGACCAC
TATTTTGACAATACGCCCTGCACATATAGTATGACGGTAAAACTCGATATTTCTAAGTTG
AAAAAGGATGGAAAAAAATTATATCCGACTCTCTTATATGGGGTTACAACAATCCTCAAT
CGACACGAAGAGTTCAGGACGGCATTAGATAAAAACGGACAGGTAGGTGTTTTTTTCAGA
AATGCTGCCTTGCTACACAATCTTTCATAA

>contig_5026:14.0-10236.0_9_nucl
ATGCTGCCTTGCTACACAATCTTTCATAAGGAAACTGAAACCTTTTCGAGTATTTGGACT
GAGTTTACAGCAGACTATACTGAGTTTCTTCAGAACTATCAAAAGGATATAGACGCTTAT
GGTGAACGAAAGGGAATGTTCGCAAAGCCTAATCCTCCGGAAAACACTTTCCCTGTTTCT
ATGATACCGTGGACAAGCTTTGAAGGCTTTAACTTAAATCTAAAAAAAGGATATGACTAT
CTACTGCCGATATTTACGTTTGGGAAGTATTATGAGGATGGCGGAAAATACTATATTCCC
TTATCGATTCAAGTGCATCATGCCGTTTGTGATGGCTTTCATGTTTGCCGTTTTTTGGAT
GAATTACAAGACTTGCTGAATAAATAA

>contig_5026:14.0-10236.0_8_prot
MMQFTKIDINNWTRKEYFDHYFDNTPCTYSMTVKLDISKLKKDGKKLYPTLLYGVTTILN
RHEEFRTALDKNGQVGVFFRNAALLHNLS*

>contig_5026:14.0-10236.0_9_prot
MLPCYTIFHKETETFSSIWTEFTADYTEFLQNYQKDIDAYGERKGMFAKPNPPENTFPVS
MIPWTSFEGFNLNLKKGYDYLLPIFTFGKYYEDGGKYYIPLSIQVHHAVCDGFHVCRFLD
ELQDLLNK*
```

Nucleotide sequences were extracted from the relevant part of the contig:
```bash
# grep for the contig ID + next line (seq) | get seq |
# cut into 100bp chunks | extract relevant rows
grep -A 1 "contig_5026:14.0-10236.0$" /scratch/users/vgalata/gdb/results/assembly/lr/flye/ASSEMBLY.POLISHED.fasta | tail -n 1 | sed -r 's/(.{100})/\1\n/g' | awk 'NR > 55 && NR < 63 {print $0}'
```
and checked by translating them using the [ExPASy tool](https://web.expasy.org/translate/).

These proteins clustered only with one other protein, also from `Flye` assembly:
```bash
grep -A 1 "contig_5026:14.0-10236.0_8" /scratch/users/vgalata/gdb/results/analysis/mmseqs2/clusters.tsv 
# flye__contig_5026:14.0-10236.0_8        flye__contig_5026:14.0-10236.0_8
# flye__contig_5027:1.0-43229.0_23        flye__contig_5027:1.0-43229.0_23
grep -A 1 "contig_5026:14.0-10236.0_9" /scratch/users/vgalata/gdb/results/analysis/mmseqs2/clusters.tsv 
# flye__contig_5026:14.0-10236.0_9        flye__contig_5026:14.0-10236.0_9
# flye__contig_5027:1.0-43229.0_24        flye__contig_5027:1.0-43229.0_24
```
- both clustered proteins are neighbors from the same contig
- both clustered proteins have no `RGI` hit

MetaT coverage for the contig region containing both genes:
```bash
grep -P "^contig_5026:14\.0-10236\.0\t" /scratch/users/vgalata/gdb/results/mapping/metat/lr/flye/ASSEMBLY.POLISHED.sr.cov.perbase | awk -F'\t' '$2 >= 5547 && $2 <= 6174 {print $0}' | less
```

- both genes are covered by metaT

## MSA w/ ARO:3004454

Multiple-sequence alignment using [EMBOSS Clustal Omega](https://www.ebi.ac.uk/Tools/msa/clustalo/) (`CLUSTAL O(1.2.4)`).

```
Protein alignment:

contig_5026:14.0-10236.0_8_prot      MMQFTKIDINNWTRKEYFDHYFDNTPCTYSMTVKLDISKLKKDGKKLYPTLLYGVTTILN	60
gb|AAA23018.1|+|Campylobacter        -MQFTKIDINNWTRKEYFDHYFGNTPCTYSMTVKLDISKLKKDGKKLYPTLLYGVTTIIN	59
contig_5026:14.0-10236.0_9_prot      ------------------------------------------------------------	0
                                                                                                 

contig_5026:14.0-10236.0_8_prot      RHEEFRTALDKNGQVGVFFRNAALLHNLS*------------------------------	89
gb|AAA23018.1|+|Campylobacter        RHEEFRTALDENGQVGVFSEMLPCYTVFHKETETFSSIWTEFTADYTEFLQNYQKDIDAF	119
contig_5026:14.0-10236.0_9_prot      --------------------MLPCYTIFHKETETFSSIWTEFTADYTEFLQNYQKDIDAY	40
                                                                :                                

contig_5026:14.0-10236.0_8_prot      ------------------------------------------------------------	89
gb|AAA23018.1|+|Campylobacter        GERMGMSAKPNPPENTFPVSMIPWTSFEGFNLNLKKGYDYLLPIFTFGKYYEEGGKYYIP	179
contig_5026:14.0-10236.0_9_prot      GERKGMFAKPNPPENTFPVSMIPWTSFEGFNLNLKKGYDYLLPIFTFGKYYEDGGKYYIP	100
                                                                                                 

contig_5026:14.0-10236.0_8_prot      -----------------------------	89
gb|AAA23018.1|+|Campylobacter        LSIQVHHAVCDGFHVCRFLDELQDLLNK-	207
contig_5026:14.0-10236.0_9_prot      LSIQVHHAVCDGFHVCRFLDELQDLLNK*	128


DNA alignment:

contig_5026:14.0-10236.0_8_nucl          ATGATGCAATTCACAAAGATTGATATAAATAATTGGACACGAAAAGAGTATTTCGACCAC	60
gb|M35190.1|+|309-932|Campylobacter      ---ATGCAATTCACAAAGATTGATATAAATAATTGGACACGAAAAGAGTATTTCGACCAC	57
contig_5026:14.0-10236.0_9_nucl          ------------------------------------------------------------	0
                                                                                                     

contig_5026:14.0-10236.0_8_nucl          TATTTTGACAATACGCCCTGCACATATAGTATGACGGTAAAACTCGATATTTCTAAGTTG	120
gb|M35190.1|+|309-932|Campylobacter      TATTTTGGCAATACGCCCTGCACATATAGTATGACGGTAAAACTCGATATTTCTAAGTTG	117
contig_5026:14.0-10236.0_9_nucl          ------------------------------------------------------------	0
                                                                                                     

contig_5026:14.0-10236.0_8_nucl          AAAAAGGATGGAAAAAAATTATATCCGACTCTCTTATATGGGGTTACAACAATCCTCAAT	180
gb|M35190.1|+|309-932|Campylobacter      AAAAAGGATGGAAAAAAGTTATACCCAACTCTTTTATATGGAGTTACAACGATCATCAAT	177
contig_5026:14.0-10236.0_9_nucl          ------------------------------------------------------------	0
                                                                                                     

contig_5026:14.0-10236.0_8_nucl          CGACACGAAGAGTTCAGGACGGCATTAGATAAAAACGGACAGGTAGGTGTTTTTTTCAGA	240
gb|M35190.1|+|309-932|Campylobacter      CGACATGAAGAGTTCAGGACCGCATTAGATGAAAACGGACAGGTAGGCGTTTTT-TCAGA	236
contig_5026:14.0-10236.0_9_nucl          ------------------------------------------------------------	0
                                                                                                     

contig_5026:14.0-10236.0_8_nucl          AATGCTGCCTTGCTACACAATCTTTCATAA------------------------------	270
gb|M35190.1|+|309-932|Campylobacter      AATGCTGCCTTGCTACACAGTTTTTCATAAGGAAACTGAAACCTTTTCGAGTATTTGGAC	296
contig_5026:14.0-10236.0_9_nucl          -ATGCTGCCTTGCTACACAATCTTTCATAAGGAAACTGAAACCTTTTCGAGTATTTGGAC	59
                                          ****************** * ********                              

contig_5026:14.0-10236.0_8_nucl          ------------------------------------------------------------	270
gb|M35190.1|+|309-932|Campylobacter      TGAGTTTACAGCAGACTATACTGAGTTTCTTCAGAACTATCAAAAGGATATAGACGCTTT	356
contig_5026:14.0-10236.0_9_nucl          TGAGTTTACAGCAGACTATACTGAGTTTCTTCAGAACTATCAAAAGGATATAGACGCTTA	119
                                                                                                     

contig_5026:14.0-10236.0_8_nucl          ------------------------------------------------------------	270
gb|M35190.1|+|309-932|Campylobacter      TGGTGAACGAATGGGAATGTCCGCAAAGCCTAATCCTCCGGAAAACACTTTCCCTGTTTC	416
contig_5026:14.0-10236.0_9_nucl          TGGTGAACGAAAGGGAATGTTCGCAAAGCCTAATCCTCCGGAAAACACTTTCCCTGTTTC	179
                                                                                                     

contig_5026:14.0-10236.0_8_nucl          ------------------------------------------------------------	270
gb|M35190.1|+|309-932|Campylobacter      TATGATACCGTGGACAAGCTTTGAAGGCTTTAACTTAAATCTAAAAAAAGGATATGACTA	476
contig_5026:14.0-10236.0_9_nucl          TATGATACCGTGGACAAGCTTTGAAGGCTTTAACTTAAATCTAAAAAAAGGATATGACTA	239
                                                                                                     

contig_5026:14.0-10236.0_8_nucl          ------------------------------------------------------------	270
gb|M35190.1|+|309-932|Campylobacter      TCTACTGCCGATATTTACGTTTGGGAAGTATTATGAGGAGGGCGGAAAATACTATATTCC	536
contig_5026:14.0-10236.0_9_nucl          TCTACTGCCGATATTTACGTTTGGGAAGTATTATGAGGATGGCGGAAAATACTATATTCC	299
                                                                                                     

contig_5026:14.0-10236.0_8_nucl          ------------------------------------------------------------	270
gb|M35190.1|+|309-932|Campylobacter      CTTATCGATTCAAGTGCATCATGCCGTTTGTGACGGCTTTCATGTTTGCCGTTTTTTGGA	596
contig_5026:14.0-10236.0_9_nucl          CTTATCGATTCAAGTGCATCATGCCGTTTGTGATGGCTTTCATGTTTGCCGTTTTTTGGA	359
                                                                                                     

contig_5026:14.0-10236.0_8_nucl          ----------------------------	270
gb|M35190.1|+|309-932|Campylobacter      TGAATTACAAGACTTGCTGAATAAATAA	624
contig_5026:14.0-10236.0_9_nucl          TGAATTACAAGACTTGCTGAATAAATAA	387
```

- also saved in `lr_flye__ARO3004454_contig_5026:14.0-10236.0__clustalo_prot.txt`
- alignments: almost perfect, only few mismatches/gaps
- both CDS/protein sequences cover the complete ARO sequence

`contig_5026:14.0-10236.0_8` and `contig_5026:14.0-10236.0_9` have an overlap of 29 bps: `ATGCTGCCTTGCTACACAATCTTTCATAA`:
```
AATGCTGCCTTGCTACACAATCTTTCATAA.
.N..A..A..L..L..H..N..L..S..*.
.ATGCTGCCTTGCTACACAATCTTTCATAAG
..M..L..P..C..Y..T..I..F..H..K.
```