# Access for submitted metaP data

Credentials required for data access as long as the submission is not public:

**ProteomeXchange accession**: PXD025505
**Username**: reviewer_pxd025505@ebi.ac.uk
**Password**: tj66CIfK

The submission includes the raw proteomic data, the spectra data and identifications files for each analyzed protein FASTA file.

# Code
Code used to generate metaP reports which are included in the report workflow.

The reports were generated for
- proteins from each assembly separately
- all proteins from all assemblies together

```bash
##Generate parameters file
java-cp SearchGUI-3.3.20.jar eu.isas.searchgui.cmd.IdentificationParametersCLI \
    -out "params.par" \
    -db "database.fasta" \
    -prec_tol 10 \
    -prec_ppm 1 \
    -frag_tol 15 \
    -frag_ppm 1 \
    -digestion 0 \
    -enzyme "Trypsin" \
    -specificity 0 \
    -mc 2 \
    -fixed_mods "Carbamidomethylation of C" \
    -variable_mods "Oxidation of M" \
    -min_charge 2 \
    -max_charge 4 \
    -fi "b" \
    -ri "y" \
    -min_isotope 0 \
    -max_isotope 1 \
    -import_peptide_length_min 7 \
    -import_peptide_length_max 60 \
    -xtandem_noise_suppr 0 \
    -xtandem_quick_acetyl 1 \
    -xtandem_quick_pyro 1 \
    -xtandem_refine 1 \
    -msgf_instrument 1 \
    -msgf_fragmentation 1 \
    -msgf_protocol 0 \
    -msgf_min_pep_length 7 \
    -msgf_max_pep_length 60 \
    -msgf_num_ptms 2 \
    -comet_num_ptms 2
#SearchGUI search
java -cp SearchGUI-3.3.20.jar eu.isas.searchgui.cmd.SearchCLI \
    -spectrum_files "MGF_DIR" \
    -output_folder "OUT_DIR" \
    -id_params "params.par" \
    -xtandem 1 \
    -msgf 1 \
    -comet 1 \
    -andromeda 0 \
    -output_default_name "Sample_Name" \
    -output_option 0 \
    -output_data 1 \
    -output_date 1 
#PeptideShaker analysis
java -cp PeptideShaker-1.16.45.jar eu.isas.peptideshaker.cmd.PeptideShakerCLI \
    -experiment "Sample_Name" \
    -sample "1" \
    -replicate "1" \
    -identification_files "SearchGUI_Outpit" \
    -out "Sample_Name.cpsx" \
    -zip "Sample_Name.cpsx.zip" 
    
#PeptideShaker reports.
java -cp PeptideShaker-1.16.45.jar eu.isas.peptideshaker.cmd.ReportCLI \
    -in "Sample_Name.cpsx.zip" \
    -out_reports "OUT_DIR" \
    -reports 6,9 
    
#Mzid Generation
java -cp PeptideShaker-1.16.45.jar eu.isas.peptideshaker.cmd.MzidCLI \
    -in "Sample_Name.cpsx.zip" \
    -output_file "Sample_Name.mzid" \
    -contact_first_name Benoit \
    -contact_last_name Kunath \
    -contact_email benoit.j.kunath@gmail.com \
    -contact_address avenue de l’Université 7 Belval Luxembourg \
    -organization_name LCSB \
    -organization_email benoit.kunath@uni.lu \
    -organization_address avenue de l’Université 7 Belval Luxembourg
```