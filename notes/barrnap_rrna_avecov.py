#!/usr/bin/env python

# Given GFF file from barrnap and per-base coverage file, compute the average coverage for the features in the GFF file
# ARGS:
#   1) GFF file
#   2) cov. file
#   3) output file

import os
import sys
import pandas

def parse_barrnap_gff(ifile_path):
    """
    Parse GFF3 files created by barrnap (rRNA gene prediction from given contigs)
    GFF format: https://www.ensembl.org/info/website/upload/gff.html
    """
    # import pandas
    genes = pandas.read_csv(ifile_path, sep="\t", comment="#", header=None, names=["contig_id", "source", "feature", "start", "end", "score", "strand", "frame", "info"])
    genes = genes[["contig_id", "start", "end", "strand", "info"]]
    genes["prot_num"] = genes["start"].astype(str) + "_" + genes["end"].astype(str)
    genes["prot_id"]  = genes["contig_id"] + "__" + genes["prot_num"]
    assert (genes.start < genes.end).all() # TODO: TEST
    return genes

def ave_gene_cov(cov_file, genes):
    """
    Compute average coverage from given per-base coverage file and a list of genes
    Genes must be in a table as created by parse_prodigal_faa_headers()
    """
    genes = genes.sort_values(by=["contig_id", "prot_num"])
    genes = genes.assign(ave_cov=None)
    with open(cov_file, "r") as ifile:
        last_contig_id  = ""
        last_contig_cov = [] # array of cov. values for bases 1..N (idx 0..N-1)
        for line in ifile:
            contig_id, base, coverage = line.strip().split("\t")
            if contig_id != last_contig_id:
                # ave. cov. for genes from this contig
                contig_genes = genes.contig_id == last_contig_id
                if contig_id != "":
                    genes.loc[contig_genes, "ave_cov"] = genes.loc[contig_genes,:].apply(lambda x: sum(last_contig_cov[(x.start-1):x.end]) / (x.end-x.start+1), axis=1)
                # reset
                last_contig_id  = contig_id
                last_contig_cov = []
            last_contig_cov.append(float(coverage))
    return genes

if __name__ == "__main__":
    print("GFF: %s, COV: %s, OUTPUT: %s" % (sys.argv[1], sys.argv[2], sys.argv[3]))
    genes = parse_barrnap_gff(sys.argv[1])
    genes.drop_duplicates(keep='first', inplace=True)
    genes = ave_gene_cov(sys.argv[2], genes)
    genes.to_csv(sys.argv[3], sep="\t", header=True, index=False, index_label=False)
